<?php
session_start();
ob_start();
include_once("elements/dbConnect.php");
 
 if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_SESSION['user']))
 {
     
     if (isset($_POST['wID']))
     {
         
             
             $uid =$_SESSION['user'];
             $wid= $_POST['wID'];
             
            $stmt=$conn->prepare("
            SELECT wID
            FROM watchlist
            WHERE wID = ? and userID=?;
            ");

            $stmt->bind_param("ii", $wid, $uid);
             
             echo $wid."   ".$uid;

            $stmt->execute();

            $result=$stmt->get_result();        

            // echo $result->num_rows;
            echo "<div id='resp'>";
            if(($result->num_rows) == 1)   
            {
                    $stmt=$conn->prepare("
                    DELETE FROM watchlist
                    WHERE userID = ? and wID = ?;
                    ");

                    $stmt->bind_param("ii", $uid, $wid);                   

                    $stmt->execute();
                
                header("Location: watchlist.php");
            }
             else
             {
                 echo "A kiválaszott film már törölve lett.";
             }
             echo "</div>";
        
     }
     
    if (isset($_POST['mID']))
    {
        $id = $_POST['mID'];
        
            
              
            $uid = $_SESSION['user'];
            
            $stmt=$conn->prepare("
            SELECT wID
            FROM watchlist
            WHERE movieID = ?
            and
            userID = ?;
            ");

            $stmt->bind_param("ii", $id,$_SESSION['user']);

            $stmt->execute();

            $result=$stmt->get_result();        

            if(($result->num_rows) == 0)
            {
                $stmt=$conn->prepare("
                INSERT INTO watchlist (userID, movieID, dateAdded) 
                VALUES (?,?, NOW() );
                ");
                                
                $stmt->bind_param("ii",$uid, $id);
                $stmt->execute();
                echo "Sikereresen hozzáadva!";          
            }
            else
            {
                echo "Már hozzá lett adva";
            }                   
            
        
    }
 }
$conn->close();
?>

