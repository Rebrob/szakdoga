<!DOCTYPE html>
<html>
    <head>
<?php require_once("elements/scriptLink.php"); ?>
    </head>
    
    <body>
<?php require_once("elements/navbar.php"); ?>
<div class="container">
     <div class="row justify-content-center">
         <div class="col-sm-6   ">
<?php


include_once("elements/dbConnect.php");


if ($_SERVER["REQUEST_METHOD"] == "GET")
{
    if(isset($_GET['logout']))
    {
        if($_GET['logout'] == 1)
        {
            session_unset();
            session_destroy();

            header("location:login.php");
            
        }
    }
    
    if (isset($_GET['error']))
    {
        echo "<div class='bg-warning text-center'>A funkció használatához előbb be kell jelentkezned!</div><br>";
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $name = $_POST['username'];
    $password = $_POST['password'];    
    
    $stmt=$conn->prepare("
    SELECT uName, uPwd, uID  FROM accounts where accounts.uName = ?
    ");    
    $stmt->bind_param("s", $name);
    $stmt->execute();
    $result = $stmt->get_result();
    
    
    if ($result->num_rows == 1) 
    {
        $row = mysqli_fetch_array($result);
        

        if (password_verify($password, $row[1])) 
            {          
            $_SESSION["user"] = $row['uID'];
            $stmt=$conn->prepare("UPDATE accounts SET uLastLogin=CURRENT_TIMESTAMP WHERE uID = ?");
            $stmt->bind_param("i", $row['uID']);
            $stmt->execute();
            $result=$stmt->get_result();
            header("Location:".htmlentities($_SERVER['PHP_SELF']));
            } 
            else 
            {
                echo "<div class='bg-warning'>Hibás felhasználónév vagy jelszó!</div>";
            }
    } 
    else
    {
          echo "<div class='bg-warning'>Hibás felhasználónév vagy jelszó!</div>";
    }
}

?>


<?php 
if (!isset($_SESSION["user"]))
{
?>
<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">

    Felhasználónév: <input class="form-control" type="text" name="username"  required> <br>
    Jelszó: <input class="form-control" type="password" name="password"  required> <br>    
    <button class="btn btn-success" type="submit" value="Submit">Belép</button> <br> 
</form>


        
<?php 
}
else {echo "<a href='login.php?logout=1'><button class='form-control btn btn-success'>Kijelenkezés</button></a>";}
?>
        </div>
         </div>
    </div>
        </body>
</html>
