<?php 

require_once("elements/dbConnect.php");
ob_start();
$pageTitle="movie";
?>
    <!DOCTYPE html>
<html>
    <head>

        <?php require_once("elements/scriptLink.php"); ?>
            <script src="scripts/sliderVal.js"></script>
            <title>
                <?php echo $pageTitle ?>
            </title>

    </head>
    <body>
        <?php require_once("elements/navbar.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <?php
    $pageTitle="movie";

    if (isset($_GET['id']))
    {              
        $id=$_GET['id'];          

        if (isset($_SESSION['user']) and $_SESSION['user']==1)
        {
            echo "<a href='manager_movie.php?id=".$id."'>Szerkeszt</a><br><br>";
        }

        $stmt = $conn->prepare("SELECT * FROM movies WHERE mID= ? ;");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) // Ha van eredmény, akkor...
        {        
            require_once("mParts/loadRating.php"); ///////// Értékelés load

            if (isset($_SESSION['user']))
            {            
                $currentRate=NULL;

                require_once("mParts/loadUserRating.php"); // load user own rating into $currentRate
                require_once("mParts/insertRating.php");   ///// értékelés beszúr, frissít, töröl         

                                ?>
                                <h4> Értékelésed: <?php if(isset($currentRate)) {echo $currentRate;}?> </h4>
                                <form class="form-group" action="" method="post">
                                    <input class="form-control-range" type="range" min="0" max="100" value="<?php if(isset($currentRate)) {echo $currentRate;}?>" class="slider" id="slider" name="rating">
                                    <h3 id="displayRating"> <?php if(isset($currentRate)) {echo $currentRate;}?> </h3>
                                    <button class="btn btn-primary" name="sendRating">Értékel</button>
                                </form>
                                <br>
                                <?php
            }
                                ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <?php
            
            $row = $result->fetch_assoc();

            if(!empty($row['titleHun']))
            {
                echo "<h1>".$row['titleHun']."</h1><br>";
            }
            else
            {
                echo "<h1>".$row['titleOriginal']."</h1><br>";
            }

            echo "<table class='table'>";   
                echo "<tr><td><b>Eredeti cím:</b></td><td>".$row['titleOriginal']."</td></tr>";
                echo "<tr><td><b>Megjelenés éve:</b></td><td> ".$row['year']."</td></tr>";
                echo "<tr><td><b>Hossz:</b> </td><td>".$row['runtime']." perc </td></tr>";        
                echo "<tr><td><b>korhatár:</b></td><td> ".$row['ageLimit']."</td></tr>";        

                $pageTitle=$row['titleHun'];
            ?>
                                <script>
                                    document.title = '<?php echo $row['titleHun'] ?>';
                                </script>
                                <?php

                $link=$row["trailerLink"];        
                 if ( is_null( $link ) )
                 {
                    echo "<tr><td><b>Előzetes: </b></td><td> Nincs még előzetes hozzáadva.</td></tr>";
                 }
                 else
                 {
                    echo "<tr><td><b>Előzetes:</b></td><td> <a href='https://www.youtube.com/$link'>YOUTUBE</a></td></tr>";
                 }

                require_once("mParts/loadSeries.php");  //Sorozat betöltése 

                if ( is_null($row['description']) )
                {
                    echo "<tr><td><b>Leírás:</b></td><td> Nincs még leírás hozzáadva.</td></tr><br>";
                }
                else
                {
                       echo "<tr><td><b>Leírás:</b></td><td> ".$row['description']."</td></tr><br>";     
                }

            echo "</table>";

            require_once("mParts/loadGenre.php");  //Kategória betöltése       
            require_once("mParts/loadImages.php");  //képek betöltése

            ?>
                        </div>
                        <div class="col-sm-6 text-center">
                            <?php        
                require_once("mParts/loadCover.php");  //Borító betöltése

            if (isset($_SESSION['user'])) //Ha be van jeletkezve, akkor....
            {
                $userID=$_SESSION['user'];
               require_once("mParts/loadWatchlist.php"); // megnézendőhöz ad, status
            }
            
                require_once("mParts/loadCrew.php");  //Stáblista betöltése

            ?>
                        </div>
                    </div>
                    <div class="row justify-content-center">

                        <div class="col-sm-6">
                            <?php
            if (isset($_SESSION['user'])) //Ha be van jeletkezve, akkor....
            {
                $userID=$_SESSION['user'];
               require_once("mParts/loadWatchlist.php"); // megnézendőhöz ad, status

                ?>       
                                    <br>
                                    <br>
                                    <h3>Új hozzászólás:</h3>
                                    <div class="form-group">
                                        <form action="" method="post">
                                            <textarea class="form-control" name="addComment"></textarea>
                                            <br>
                                            <button class="btn btn-success" type="submit" name="addSubmit">Hozzászól</button>
                                        </form>
                                    </div>
                                    <?php                    
            }       

          require_once("mParts/loadComments.php"); //kommentek betöltése           
        } 
        else 
        {
           header("Location:404.php?err=pnf");
        }
            require_once("mParts/insertComment.php");        //komment beszúrása
    }

                        $conn->close();
?>
                                    <script>
                                        <?php include_once("scripts/watchlistAJAX.js") ?>
                                    </script>
                    </div>
                </div>
            </div>
    </body>
</html>