-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2019 at 05:25 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `szakdolgozat`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `uID` int(11) NOT NULL,
  `uName` varchar(24) COLLATE utf8_hungarian_ci NOT NULL,
  `uPwd` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `regDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uBorn` date NOT NULL,
  `isFemale` int(11) NOT NULL,
  `uMail` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `uLastLogin` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`uID`, `uName`, `uPwd`, `regDate`, `uBorn`, `isFemale`, `uMail`, `uLastLogin`) VALUES
(1, 'admin', '$2y$10$GPKOnOHrY6TXmHyO6Kvi6uFYHMVKepIMKitAEXDM0dfJ21e1wTinW', '2019-01-24 15:24:06', '1998-12-26', 0, 'rebrob1998@gmail.com', '2019-03-19 08:54:36'),
(2, 'user', '$2y$10$f0tjq6x1c7iL0qyFIlUGb.TwtN5x5ktvxAlYFwZ6438u0/VBHSL2K', '2019-01-24 15:26:57', '2000-01-01', 0, 'user@usermail.com', '2019-02-25 17:52:57'),
(3, 'child', '$2y$10$vLqBqr8wA/uS/So22TshiemebYqECJIRZAIumHDPiKxDrJu.jzbHa', '2019-01-24 15:29:31', '2010-01-01', 0, 'child@child.com', NULL),
(4, 'rebrob', '$2y$10$o/gJ1ZTmYYHiqoYp9p4MkOrUdRB2XjVkbha/Qg0d7dWiHP1dsx5QO', '2019-03-10 19:35:25', '1998-12-26', 0, 'rebrob.robtm98@gmail.com', '2019-03-19 08:36:32');

-- --------------------------------------------------------

--
-- Table structure for table `actor_role`
--

CREATE TABLE `actor_role` (
  `m_nID` int(11) NOT NULL,
  `role` varchar(50) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `actor_role`
--

INSERT INTO `actor_role` (`m_nID`, `role`) VALUES
(5, 'Galadriel'),
(6, 'Saruman'),
(9, 'Legolas'),
(10, 'Boromir'),
(11, 'Aragorn'),
(12, 'Arwen'),
(15, 'Daenerys Targaryen'),
(18, 'Ned Stark'),
(19, 'Fiona'),
(20, 'Shrek');

-- --------------------------------------------------------

--
-- Table structure for table `contributed`
--

CREATE TABLE `contributed` (
  `conID` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `year` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contributed`
--

INSERT INTO `contributed` (`conID`, `title`, `year`, `userID`, `added`) VALUES
(3, 'Indul a bakterház', 1980, 1, '2019-03-16 17:47:56'),
(5, '9', 2009, 1, '2019-03-16 17:55:04');

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `gID` int(11) NOT NULL,
  `gName` varchar(30) COLLATE utf8_hungarian_ci NOT NULL,
  `genreDateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`gID`, `gName`, `genreDateAdded`) VALUES
(1, 'Akció', '2019-01-24 17:11:54'),
(2, 'Kaland', '2019-01-24 17:11:54'),
(3, 'Sci-fi', '2019-01-24 17:11:54'),
(4, 'Fantasy', '2019-01-24 17:11:54'),
(5, 'Családi', '2019-01-24 17:11:54'),
(6, 'Dráma', '2019-02-19 17:35:29'),
(7, 'Krimi', '2019-03-18 10:28:05'),
(8, 'Thriller', '2019-03-18 10:28:05'),
(9, 'Animációs', '2019-03-18 10:56:03'),
(10, 'Vígjáték', '2019-03-18 10:56:49');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `jID` int(11) NOT NULL,
  `jName` varchar(30) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`jID`, `jName`) VALUES
(1, 'Rendező'),
(5, 'Színész'),
(10, 'Zene'),
(15, 'Író'),
(20, 'Producer'),
(25, 'Forgatókönyvíró');

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `mID` int(11) NOT NULL,
  `titleHun` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `titleOriginal` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `year` int(11) NOT NULL,
  `runtime` int(11) DEFAULT NULL,
  `isSeries` tinyint(4) NOT NULL DEFAULT '0',
  `trailerLink` varchar(50) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `movieDateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text COLLATE utf8_hungarian_ci,
  `updatedOn` datetime DEFAULT NULL,
  `ageLimit` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`mID`, `titleHun`, `titleOriginal`, `year`, `runtime`, `isSeries`, `trailerLink`, `movieDateAdded`, `description`, `updatedOn`, `ageLimit`) VALUES
(1, 'A Gyűrűk Ura: A Gyűrű Szövetsége', 'The Lord of the Rings: The Fellowship of the Ring', 2001, 178, 0, 'watch?v=V75dMMIW2B4', '2019-01-24 18:15:07', 'szia', '2019-03-16 17:28:50', 12),
(2, 'Csillagok között', 'Interstellar', 2014, 178, 0, 'watch?v=zSWdZVtXT7E', '2019-01-24 18:17:41', 'A Csillagok között egy 2014-es tudományos-fantasztikus kalandfilm, amely felvonultatja a 21. századi elméleti fizika elképzeléseit a világűrről, a tér és idő viszonyáról, valamint felvázolja a zsákutcába jutott emberiség lehetséges fejlődési útját, jövőjét.', '2019-03-03 15:30:13', 12),
(3, 'Trónok harca', 'Game of Thrones', 2011, 57, 1, NULL, '2019-01-24 18:19:55', NULL, '2019-03-04 11:52:20', 18),
(4, NULL, 'The Walking Dead', 2010, 45, 1, NULL, '2019-03-05 11:22:44', NULL, '2019-03-05 11:23:39', 18),
(5, 'Totál szívás', 'Breaking Bad', 2008, 50, 1, NULL, '2019-03-18 11:23:24', NULL, '2019-03-18 11:26:31', 18),
(6, NULL, 'Shrek', 2001, 90, 0, NULL, '2019-03-18 11:55:11', NULL, '2019-03-18 11:55:37', 6);

-- --------------------------------------------------------

--
-- Table structure for table `movie_comments`
--

CREATE TABLE `movie_comments` (
  `cID` int(11) NOT NULL,
  `movieID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `commentText` text COLLATE utf8_hungarian_ci NOT NULL,
  `commentDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `movie_comments`
--

INSERT INTO `movie_comments` (`cID`, `movieID`, `userID`, `commentText`, `commentDate`) VALUES
(1, 1, 2, 'Nagyon jó film, imádtam :)\r\n100/100', '2019-02-21 17:04:07'),
(2, 1, 3, 'Natyon teccet', '2019-02-21 17:04:07'),
(3, 2, 2, 'Elgondolkodtató film, nagyon jó volt.', '2019-02-21 17:04:07'),
(4, 1, 1, 'szia norbiiiiiiiiii', '2019-03-07 11:17:11'),
(5, 1, 1, 'norika itt van', '2019-03-07 11:17:48'),
(6, 1, 1, 'szia <br />\r\n1<br />\r\n2<br />\r\n3', '2019-03-07 12:04:14'),
(7, 6, 4, 'Egyik kedvenc filmem!', '2019-03-18 12:05:29');

-- --------------------------------------------------------

--
-- Table structure for table `movie_genres`
--

CREATE TABLE `movie_genres` (
  `mgID` int(11) NOT NULL,
  `movieID` int(11) NOT NULL,
  `genreID` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `movie_genres`
--

INSERT INTO `movie_genres` (`mgID`, `movieID`, `genreID`, `dateAdded`) VALUES
(21, 2, 6, '2019-03-04 11:50:13'),
(22, 3, 4, '2019-03-04 11:51:04'),
(26, 1, 4, '2019-03-04 12:09:16'),
(27, 1, 5, '2019-03-04 12:09:21'),
(28, 2, 3, '2019-03-04 12:09:49'),
(29, 4, 3, '2019-03-05 11:23:50'),
(30, 4, 1, '2019-03-05 11:23:53'),
(31, 3, 1, '2019-03-09 18:43:39'),
(32, 5, 1, '2019-03-18 11:26:53'),
(33, 5, 7, '2019-03-18 11:28:20'),
(34, 5, 8, '2019-03-18 11:28:27'),
(35, 6, 9, '2019-03-18 11:56:14'),
(36, 6, 5, '2019-03-18 11:56:19'),
(37, 6, 10, '2019-03-18 11:56:55');

-- --------------------------------------------------------

--
-- Table structure for table `movie_images`
--

CREATE TABLE `movie_images` (
  `imgID` int(11) NOT NULL,
  `movieID` int(11) NOT NULL,
  `isCover` tinyint(1) NOT NULL,
  `imgLocation` varchar(200) COLLATE utf8_hungarian_ci NOT NULL,
  `imgAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `movie_images`
--

INSERT INTO `movie_images` (`imgID`, `movieID`, `isCover`, `imgLocation`, `imgAdded`) VALUES
(1, 1, 1, 'images/movies/cover/1552239095.jpg', '2019-03-10 18:26:44'),
(3, 1, 0, 'images/movies/base/1552239136.JPG', '2019-03-10 18:32:16'),
(4, 1, 0, 'images/movies/base/1552239168.jpg', '2019-03-10 18:32:48'),
(5, 2, 1, 'images/movies/cover/1552240116.jpg', '2019-03-10 18:48:36'),
(7, 2, 0, 'images/movies/base/1552240146.jpg', '2019-03-10 18:49:06'),
(8, 2, 0, 'images/movies/base/1552241074.jpg', '2019-03-10 19:04:34'),
(9, 4, 1, 'images/movies/cover/1552842154.jpg', '2019-03-17 18:02:34');

-- --------------------------------------------------------

--
-- Table structure for table `movie_names`
--

CREATE TABLE `movie_names` (
  `mnID` int(11) NOT NULL,
  `movieID` int(11) NOT NULL,
  `nameID` int(11) NOT NULL,
  `jobID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `movie_names`
--

INSERT INTO `movie_names` (`mnID`, `movieID`, `nameID`, `jobID`) VALUES
(5, 1, 6, 5),
(6, 1, 2, 5),
(7, 1, 10, 1),
(8, 1, 10, 20),
(9, 1, 1, 5),
(10, 1, 3, 5),
(11, 1, 4, 5),
(12, 1, 5, 5),
(13, 1, 11, 15),
(15, 3, 12, 5),
(18, 3, 3, 5),
(19, 6, 14, 5),
(20, 6, 13, 5);

-- --------------------------------------------------------

--
-- Table structure for table `names`
--

CREATE TABLE `names` (
  `nID` int(11) NOT NULL,
  `firstName` varchar(60) COLLATE utf8_hungarian_ci NOT NULL,
  `lastName` varchar(60) COLLATE utf8_hungarian_ci NOT NULL,
  `born` date NOT NULL,
  `died` date DEFAULT NULL,
  `isFemale` tinyint(4) NOT NULL DEFAULT '0',
  `bio` text COLLATE utf8_hungarian_ci,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `names`
--

INSERT INTO `names` (`nID`, `firstName`, `lastName`, `born`, `died`, `isFemale`, `bio`, `dateAdded`, `updatedOn`) VALUES
(1, 'Orlando', 'Bloom', '1977-01-13', NULL, 0, NULL, '2019-01-24 17:53:32', NULL),
(2, 'Christopher', 'Lee', '1922-05-27', '2015-01-07', 0, 'Sir Christopher Lee, teljes nevén: Christopher Frank Carandini Lee angol színész. Az 1940-es évek végétől filmezett. A Hammer Film Productions égisze alatt forgatott horrorfilmjeivel vált ismertté. Ő volt Drakula gróf egyik leghíresebb megformálója, de eljátszotta Frankenstein teremtményét is.', '2019-01-24 17:55:50', NULL),
(3, 'Sean', 'Bean', '1959-04-17', NULL, 0, NULL, '2019-01-24 18:01:15', '2019-03-06 18:31:49'),
(4, 'Viggo', 'Mortensen', '1958-10-20', NULL, 0, NULL, '2019-01-24 18:01:15', '2019-03-06 18:32:13'),
(5, 'Liv', 'Tyler', '1977-07-01', NULL, 1, 'nem szia', '2019-01-24 18:01:15', '2019-03-06 18:11:12'),
(6, 'Cate', 'Blanchett', '1969-05-14', NULL, 1, 'Cate Blanchett, teljes nevén Catherine Elise Blanchett kétszeres Oscar- és többszörös Golden Globe-díjas ausztrál színésznő és filmrendező.', '2019-01-24 18:01:15', '2019-03-06 18:32:44'),
(7, 'Mel', 'Gibson', '1956-01-03', NULL, 0, NULL, '2019-02-19 18:33:17', NULL),
(8, 'Russell', 'Crowe', '1964-04-07', NULL, 0, NULL, '2019-02-19 18:33:17', NULL),
(9, 'Steven', 'Spielberg', '1947-12-18', NULL, 0, NULL, '2019-02-19 18:33:17', NULL),
(10, 'Peter', 'Jackson', '1961-10-31', NULL, 0, NULL, '2019-02-19 18:33:17', NULL),
(11, 'J.R.R.', 'Tolkien', '1892-01-03', '1973-09-02', 0, NULL, '2019-02-19 18:33:17', NULL),
(12, 'Emilia', 'Clarke', '1986-10-23', NULL, 1, 'got', '2019-03-06 18:37:23', '2019-03-06 18:37:37'),
(13, 'Mike', 'Myers', '1963-05-25', NULL, 0, NULL, '2019-03-18 11:58:26', '2019-03-18 11:58:35'),
(14, 'Cameron', 'Diaz', '1972-09-30', NULL, 1, NULL, '2019-03-18 11:59:38', '2019-03-18 11:59:43');

-- --------------------------------------------------------

--
-- Table structure for table `name_comments`
--

CREATE TABLE `name_comments` (
  `cID` int(11) NOT NULL,
  `nameID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `commentText` text COLLATE utf8_hungarian_ci NOT NULL,
  `commentDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `name_comments`
--

INSERT INTO `name_comments` (`cID`, `nameID`, `userID`, `commentText`, `commentDate`) VALUES
(1, 3, 2, 'Nagyon jó színész, csak sajnos minden filmben korán meghal :D', '2019-02-21 17:24:03'),
(2, 1, 2, 'Legolasz, tünde szemed mit lát?', '2019-02-21 17:25:07'),
(3, 1, 3, 'Ez egy elf?', '2019-02-21 17:25:07'),
(4, 1, 1, 'Nagyon j&oacute; volt a fiiiilm', '2019-03-07 11:27:23'),
(5, 4, 1, 'J&oacute;, műk&ouml;dik.<br />\r\n2. sor', '2019-03-16 17:56:43'),
(6, 4, 1, 'J&oacute;, műk&ouml;dik.<br />\r\n2. sor', '2019-03-16 17:56:52'),
(7, 4, 1, '12345678910', '2019-03-16 17:58:00');

-- --------------------------------------------------------

--
-- Table structure for table `name_images`
--

CREATE TABLE `name_images` (
  `imgID` int(11) NOT NULL,
  `nameID` int(11) NOT NULL,
  `isCover` tinyint(4) NOT NULL,
  `imgLocation` varchar(200) COLLATE utf8_hungarian_ci NOT NULL,
  `imgAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `name_images`
--

INSERT INTO `name_images` (`imgID`, `nameID`, `isCover`, `imgLocation`, `imgAdded`) VALUES
(1, 4, 1, 'images/names/cover/1552241796.jpg', '2019-03-10 19:16:15'),
(2, 4, 0, 'images/names/base/1552241814.jpg', '2019-03-10 19:16:54'),
(4, 4, 0, 'images/names/base/1552241828.jpg', '2019-03-10 19:17:08'),
(5, 6, 1, 'images/names/cover/1552241850.jpg', '2019-03-10 19:17:30'),
(6, 6, 0, 'images/names/base/1552241856.jpg', '2019-03-10 19:17:36'),
(7, 6, 0, 'images/names/base/1552241862.png', '2019-03-10 19:17:42'),
(8, 1, 1, 'images/names/cover/1552502264.jpg', '2019-03-13 19:37:44');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `rID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `movieID` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `ratingDateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`rID`, `userID`, `movieID`, `rating`, `ratingDateAdded`) VALUES
(1, 2, 1, 100, '2019-02-22 17:44:20'),
(2, 3, 1, 80, '2019-02-22 17:44:20'),
(9, 1, 1, 78, '2019-03-19 08:59:28'),
(10, 4, 3, 84, '2019-03-12 08:29:25'),
(11, 4, 1, 50, '2019-03-12 08:32:58'),
(12, 1, 4, 50, '2019-03-17 17:25:28'),
(13, 1, 6, 71, '2019-03-18 12:01:14'),
(14, 1, 5, 83, '2019-03-18 12:02:30'),
(15, 4, 6, 93, '2019-03-18 12:05:14');

-- --------------------------------------------------------

--
-- Table structure for table `series`
--

CREATE TABLE `series` (
  `sNumOfEpisodes` int(11) DEFAULT NULL,
  `sNumOfSeasons` int(11) DEFAULT NULL,
  `movieID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `series`
--

INSERT INTO `series` (`sNumOfEpisodes`, `sNumOfSeasons`, `movieID`) VALUES
(90, 9, 3),
(100, 9, 4),
(62, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `watchlist`
--

CREATE TABLE `watchlist` (
  `wID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `movieID` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `watchlist`
--

INSERT INTO `watchlist` (`wID`, `userID`, `movieID`, `dateAdded`) VALUES
(5, 4, 1, '2019-03-12 08:25:43'),
(16, 1, 1, '2019-03-17 17:50:05'),
(19, 4, 6, '2019-03-18 12:05:16'),
(21, 1, 2, '2019-03-19 08:35:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`uID`);

--
-- Indexes for table `actor_role`
--
ALTER TABLE `actor_role`
  ADD PRIMARY KEY (`m_nID`),
  ADD KEY `mnID` (`m_nID`);

--
-- Indexes for table `contributed`
--
ALTER TABLE `contributed`
  ADD PRIMARY KEY (`conID`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`gID`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`jID`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`mID`);

--
-- Indexes for table `movie_comments`
--
ALTER TABLE `movie_comments`
  ADD PRIMARY KEY (`cID`),
  ADD KEY `movieID` (`movieID`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `movie_genres`
--
ALTER TABLE `movie_genres`
  ADD PRIMARY KEY (`mgID`),
  ADD KEY `movieID` (`movieID`),
  ADD KEY `genreID` (`genreID`);

--
-- Indexes for table `movie_images`
--
ALTER TABLE `movie_images`
  ADD PRIMARY KEY (`imgID`),
  ADD KEY `movieID` (`movieID`);

--
-- Indexes for table `movie_names`
--
ALTER TABLE `movie_names`
  ADD PRIMARY KEY (`mnID`),
  ADD KEY `movieID` (`movieID`),
  ADD KEY `actorID` (`nameID`),
  ADD KEY `jobID` (`jobID`);

--
-- Indexes for table `names`
--
ALTER TABLE `names`
  ADD PRIMARY KEY (`nID`);

--
-- Indexes for table `name_comments`
--
ALTER TABLE `name_comments`
  ADD PRIMARY KEY (`cID`),
  ADD KEY `actorID` (`nameID`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `name_images`
--
ALTER TABLE `name_images`
  ADD PRIMARY KEY (`imgID`),
  ADD KEY `actorID` (`nameID`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`rID`),
  ADD KEY `movieID` (`movieID`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`movieID`);

--
-- Indexes for table `watchlist`
--
ALTER TABLE `watchlist`
  ADD PRIMARY KEY (`wID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `movieID` (`movieID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `uID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contributed`
--
ALTER TABLE `contributed`
  MODIFY `conID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `gID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `mID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `movie_comments`
--
ALTER TABLE `movie_comments`
  MODIFY `cID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `movie_genres`
--
ALTER TABLE `movie_genres`
  MODIFY `mgID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `movie_images`
--
ALTER TABLE `movie_images`
  MODIFY `imgID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `movie_names`
--
ALTER TABLE `movie_names`
  MODIFY `mnID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `names`
--
ALTER TABLE `names`
  MODIFY `nID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `name_comments`
--
ALTER TABLE `name_comments`
  MODIFY `cID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `name_images`
--
ALTER TABLE `name_images`
  MODIFY `imgID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `rID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `watchlist`
--
ALTER TABLE `watchlist`
  MODIFY `wID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `actor_role`
--
ALTER TABLE `actor_role`
  ADD CONSTRAINT `actor_role_ibfk_1` FOREIGN KEY (`m_nID`) REFERENCES `movie_names` (`mnID`);

--
-- Constraints for table `movie_comments`
--
ALTER TABLE `movie_comments`
  ADD CONSTRAINT `movie_comments_ibfk_1` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`),
  ADD CONSTRAINT `movie_comments_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `accounts` (`uID`);

--
-- Constraints for table `movie_genres`
--
ALTER TABLE `movie_genres`
  ADD CONSTRAINT `movie_genres_ibfk_1` FOREIGN KEY (`genreID`) REFERENCES `genres` (`gID`),
  ADD CONSTRAINT `movie_genres_ibfk_2` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`);

--
-- Constraints for table `movie_images`
--
ALTER TABLE `movie_images`
  ADD CONSTRAINT `movie_images_ibfk_1` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`);

--
-- Constraints for table `movie_names`
--
ALTER TABLE `movie_names`
  ADD CONSTRAINT `movie_names_ibfk_1` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`),
  ADD CONSTRAINT `movie_names_ibfk_2` FOREIGN KEY (`nameID`) REFERENCES `names` (`nID`),
  ADD CONSTRAINT `movie_names_ibfk_3` FOREIGN KEY (`jobID`) REFERENCES `jobs` (`jID`);

--
-- Constraints for table `name_comments`
--
ALTER TABLE `name_comments`
  ADD CONSTRAINT `name_comments_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `accounts` (`uID`),
  ADD CONSTRAINT `name_comments_ibfk_2` FOREIGN KEY (`nameID`) REFERENCES `names` (`nID`);

--
-- Constraints for table `name_images`
--
ALTER TABLE `name_images`
  ADD CONSTRAINT `name_images_ibfk_1` FOREIGN KEY (`nameID`) REFERENCES `names` (`nID`);

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_ibfk_1` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`),
  ADD CONSTRAINT `ratings_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `accounts` (`uID`);

--
-- Constraints for table `series`
--
ALTER TABLE `series`
  ADD CONSTRAINT `series_ibfk_1` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`);

--
-- Constraints for table `watchlist`
--
ALTER TABLE `watchlist`
  ADD CONSTRAINT `watchlist_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `accounts` (`uID`),
  ADD CONSTRAINT `watchlist_ibfk_2` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
