<?php
$stmt = $conn->prepare("
        SELECT jobs.jName, movies.mID, movies.titleHun, actor_role.role,  jobs.jID, movie_names.mnID, names.nID
        from (movies 
        INNER JOIN movie_names on movies.mID=movie_names.movieID)
        INNER JOIN jobs on movie_names.jobID=jobs.jID
        INNER JOIN names on movie_names.nameID=names.nID
        LEFT JOIN actor_role on movie_names.mnID=actor_role.m_nID

        WHERE names.nID=?
        ORDER BY jobs.jID;
        ");

        $stmt->bind_param("i", $id);
        $stmt->execute();

        $result = $stmt->get_result();
        
        $prev="0";
        

        if ($result->num_rows>0)
        {
            echo "<table class='table'>";
            while ($row=$result->fetch_row())
            {
                echo "<tr'><td  colspan='2'>";
                if ($prev != $row[0])
                {
                echo "<br><b>".$row[0]."</b>"."<br>";
                $prev=$row[0];    
                }
                else
                {
                    $prev=$row[0];
                }
                  echo "</td></tr>";
                echo "<tr><td>  ";
                
                echo "<a href='movie.php?id=$row[1]'>";
                    echo $row[2];
                echo "</a>";
                echo "</td><td>";
                if (!is_null($row[3]))
                {
                echo  $row[3];
                }
                else
                {
                    echo "<br>";
                }
                echo "</td></tr>";
            }
             echo "</table>";
        }
        else
        {
            echo "<br>Nincs még munkásság.<br>";
        }    
?>
