<?php
 $stmt = $conn->prepare("
        SELECT accounts.uName, name_comments.userID, name_comments.commentText, name_comments.commentDate
        FROM name_comments
        INNER JOIN accounts ON accounts.uID=name_comments.userID
        WHERE name_comments.nameID=?
        ORDER by name_comments.nameID, name_comments.commentDate DESC
        ");

        $stmt->bind_param("i", $id);
        $stmt->execute();

        $result = $stmt->get_result();
       
            
        if ($result->num_rows>0)
        {
        
           while ($row=$result->fetch_row())
            {
                echo "<table class='table table-bordered'><tr class='d-flex'><td class='col-sm-8'>";
               
                echo "<b>".$row[0]."</b></td><td class='col-sm-4'> ".$row[3]."</td></tr><tr><td colspan='2'>";
                echo nl2br($row[2]."<br>")."</td></tr>";
                echo "</table><br>";
                
            }
        }
        else
        {
            echo "<br>Nincs még hozzászólás a személyhez.<br>";
        }    
?>
