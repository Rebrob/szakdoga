<?php
if(isset($_POST['addSubmit']))
        {
            $comment = nl2br(htmlentities($_POST['addComment']));
            
            if ( strlen($comment) > 10 )
            {
                $insertComment = $conn -> prepare
                    ("
                    INSERT INTO name_comments (nameID, userID, commentText, commentDate)
                    VALUES (?, ?, ?, NOW())
                    ");
                $insertComment -> bind_param("iis", $_GET['id'], $_SESSION['user'], $comment );
                $insertComment -> execute();
             
                
                header("Location: ". htmlentities($_SERVER['PHP_SELF'])."?id=".$_GET['id']);                
            }
    
        }
?>
