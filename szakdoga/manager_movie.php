<?php 
require_once("elements/dbConnect.php");
ob_start();
?>

<!DOCTYPE html>
<html>
    <head>     
        
       <?php require_once("elements/scriptLink.php"); ?>
        <script src="scripts/sliderVal.js"></script>
        <title>Megnézendők</title>
        
    </head>
    <body>   
        
       <?php require_once("elements/navbar.php"); ?>

 <!- ---------------------------------------------------------------------------------- ->
          
<div class="container">
 <div class="row justify-content-center">
                <div class="col-sm-6">                        
<?php
///// chech admin
if(!isset($_SESSION['user']) or $_SESSION['user'] != 1  )
{
    
    header("Location: login.php?error=1");
    die();
}
/////



require_once("mmParts/deleteName.php"); ////////// name delete from movie    


if (isset($_GET['id'])) /// amennyiben id van megadva-> minden részlet
{
	$id = $_GET['id'];

	require_once("mmParts/loadMovie.php"); ////////// film adatai betöltése a formba    

    require_once("mmParts/deleteGenre.php"); ////////// genre delete from movie    
    
    require_once("mmParts/deleteImages.php"); ////////// genre image from movie      
    

    ?>
        
            <a class="text-center" href="manager_movie.php">Új film hozzáadása</a>
                    
        <form action="<?php htmlentities($_SERVER['PHP_SELF']) ?>" method="post">
        <input type="hidden" name="rejtett" value="<?php echo $id; ?>">    
        <b>Hun cím: </b><input  class="form-control"  type="text" name="editHunTitle" value="<?php echo $row[1]; ?>"> <br>
        <b> Eredeti cím:</b> <input  class="form-control"  type="text" name="editOriginalTitle" value="<?php echo $row[2]; ?>" required><br>
        <b> Év:</b> <input  class="form-control"v type="number" name="editYear" value="<?php echo $row[3]; ?>" required><br>
        <b> Hossz: (perc)</b> <input  class="form-control"  type="number" name="runtime" value="<?php echo $row[4]; ?>">    <br>
       <b>  TrailerLink </b><input  class="form-control" type="text" name="trailerLink" value="<?php echo $row[6]; ?>">    <br>
       <b>  korhatár:</b> <input  class="form-control"  type="number" name="editAge" value="<?php echo $row[9]; ?>" ><br>
        <b> Leírás:</b> <textarea class="form-control" name="description" ><?php echo nl2br($row[8]); ?></textarea><br>
       <b>  Sorozat</b> <input type="checkbox" name="isSeries" value="1" <?php  if ($row[5] == 1) 	{		echo "checked";	} ?> > <br>
           
          
    <?php ///Sorozat
	if ($row[5] == 1)
	{
	
    ?>
            <br>
   <b> Évadok száma:</b> <input class="form-control"  type="number" name="editNumOfSeasons" value="<?php echo $sRow[1]; ?>">
            <br>
   <b> Epizódok száma:</b> <input class="form-control" type="number" name="editNumOfEpisodes" value="<?php echo $sRow[2]; ?>">
    <?php
	}
            ?>
    <br>
    <button class="btn btn-success" type="submit" name="editMovie">Szerkeszt</button>
    </form>
<br>
    <?php 
    echo "<div class='bg bg-info'> Hozzáadva: " . $row[7] . "</div>" ;
    ?>

    <?php
	/////
	echo "<hr>";
    require_once("mmParts/loadGenres.php"); /// load movie genres with delete
	echo "<hr>";
	//////// add movie genre to a movie (dropdown)
	$gq = $conn->prepare("
        SELECT gID, gName
        fROM genres
        ");
	$gq->execute();
	$gresult = $gq->get_result();
    ?>

    <form action="" method="post">
    <select name="gID">
    <?php
	while ($sor = $gresult->fetch_row())
	{
		echo "<option value='" . $sor[0] . "' >" . $sor[1] . "</option>";
	}
        ?>
    </select>

    <input type="hidden" name="mID" value="<?php echo $_GET['id']; ?>">
        <button type="submit" name="addGenre">+</button>
    </form>
    <?php
   
	 require_once("mmParts/insertGenre.php"); /// Genre hozzáadás insert
      echo "<hr>";
     require_once("mmParts/insertName.php"); /// név hozzáadás insert	
    echo "<hr>";
     require_once("mmParts/loadNames.php"); //// load names
	echo "<hr>";

	/////////// add names
    $nq = $conn->prepare("
        SELECT nID, CONCAT(firstName,' ',lastName) as name
        fROM names
        ORDER BY CONCAT(firstName,' ',lastName)
        ");
	$nq->execute();
	$nresult = $nq->get_result();
    
    $rq = $conn->prepare("
        SELECT jID, jName
        fROM jobs
        ORDER BY jID
        ");
	$rq->execute();
	$rresult = $rq->get_result();
    
    ?>

    <form action="" method="post">
        <select name="nID">
            <?php	while ($sor = $nresult->fetch_row())	{		echo "<option value='" . $sor[0] . "' >" . $sor[1] . "</option>";	}        ?>
        </select>
        
        <select name="jID">
            <?php	while ($sor2 = $rresult->fetch_row())	{		echo "<option value='" . $sor2[0] . "' >" . $sor2[1] . "</option>";   	}        ?>
        </select>        

        <input type="text" name="role">
        <input type="hidden" name="mID" value="<?php echo $_GET['id']; ?>">
        <button type="submit" name="addName">+</button>
    </form>
        
    <?php

	echo "<hr>";

    require_once("mmParts/loadRoles.php"); 	/////////// load szerep
    	echo "<hr>";
     require_once("mmParts/loadImages.php"); 	/////////// load Képek
    	echo "<hr>";
     require_once("mmParts/insertImages.php"); 	/////////// Insert Képek
    
    
 
}
else    { //////Különben csak alap adatok
	 ?>
     
    
    <form action="<?php htmlentities($_SERVER['PHP_SELF']) ?>" method="post">   
        <b> Eredeti cím:</b> <input class="form-control" type="text" name="newTitleOriginal" required><br>
       <b>Év:</b> <input  class="form-control" type="number" name="year" required><br>    
    <button  class="form-control btn btn-success" type="submit" name="newMovie">Hozzáad</button>
    </form>

    <?php
}

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    require_once("mmParts/editMovie.php"); ///Film Szerkeszt
    require_once("mmParts/insertNewMovie.php"); /////// Új film	
     require_once("mmParts/editRoles.php"); 	/////////// edit szerep
}

?>
    </div></div>
    </div>
             
        </body>    
</html>
       