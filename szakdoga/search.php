<!DOCTYPE html>
<html>    
    <head>
    <?php require_once("elements/scriptLink.php"); ?>
        <title>Keresés</title>
    </head>
<body>
  <?php require_once("elements/navbar.php"); ?>
    <div class="container">
        
        <form  action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="get">
<input class="form-control form-group row" type="text" name="search" placeholder="Keres...">
<button class="form-group row btn btn-primary" type="submit">Keres</button>
</form>
    
<?php
require_once("elements/dbConnect.php");

if ($_SERVER['REQUEST_METHOD'] == "GET" and isset($_GET['search']) and !empty($_GET['search']) )
{
    $search = "%".$_REQUEST['search']."%";  
    ////////////////// Film  
    
    $stmt=$conn->prepare
    ("
    SELECT mID, titleHun, titleOriginal, description
    FROM movies
    WHERE titleHun LIKE ?
    OR titleOriginal LIKE ?
    ORDER BY 1;
    ");        
    $stmt->bind_param("ss",$search, $search);    
    $stmt->execute();    
    $result=$stmt->get_result();
    
    if ($result->num_rows>0)
    {
         echo "<h2>Filmek</h2><br><br>";
     echo "<table class='table' >";
        while ($row = $result->fetch_row())
        {
            echo "<tr class='d-flex'>";   
            echo "<td class='col-sm-3 align-middle text-center'>";
                    $cImg = $conn -> prepare
                    ("
                    SELECT imgID, imgLocation, imgAdded
                    FROM movie_images
                    WHERE movieID=? and isCover=1
                     ");
                    $cImg -> bind_param( "i", $row[0]);
                    $cImg -> execute();

                    $resultmi = $cImg -> get_result();

                    if ( $resultmi -> num_rows == 1 )
                    {
                         $crow = $resultmi -> fetch_row();	

                         echo "<a href='movie.php?id=".$row[0]."'><img src='$crow[1]' width='50%'></a>";
                    }
                    else
                    {
                        echo "<a href='movie.php?id=".$row[0]."'><img src='images/movie-dummy.jpg' width='50%'></a>";
                       
                        
                    }
                
            echo "</td><td class='col-sm-9 align-middle'>";            
            if(empty($row[1]))   { $row[1] = $row[2]; }
                            echo "<a href='movie.php?id=".$row[0]."'><h2>$row[1]</h2></a>";
             echo "</td></tr>";  
            
        }
     echo "</table>";
    }    

    ////////////////// Személy   
    
    $stmt=$conn->prepare
    ("
    SELECT nID, concat(firstName,' ',lastName) as name
    FROM names
    WHERE concat(firstName,' ',lastName) LIKE ?
    ");        
    $stmt->bind_param("s",$search);    
    $stmt->execute();    
    $result=$stmt->get_result();
    
    if ($result->num_rows>0)
    {
         echo "<h2>Személyek:</h2><br><br>";
        echo "<table class='table align-middle' >";
            while ($row = $result->fetch_row())
            {
                echo "<tr class='d-flex align-middle'>";        
                echo "<td class='col-sm-3  text-center'>";
                 $cImg = $conn -> prepare
                        ("
                        SELECT imgID, imgLocation, imgAdded
                        FROM name_images
                        WHERE nameID=? and isCover=1
                         ");
                        $cImg -> bind_param( "i", $row[0]);
                        $cImg -> execute();

                        $resulti = $cImg -> get_result();

                        if ( $resulti -> num_rows == 1 )
                        {
                             $crow = $resulti -> fetch_row();	
                             echo "<a href='name.php?id=".$row[0]."'><img  src='$crow[1]' width='50%'></a>";
                        }
                        else
                        {
                            echo "<a href='name.php?id=".$row[0]."'><img class='' src='images/person-dummy.jpg' width='50%'></a>";
                        }
                        
                echo "</td><td class='col-sm-9 '>";                       
                
                echo "<a href='name.php?id=".$row[0]."'><h2>".trim($row[1])."</h2></a>";
                
        echo "</td></tr>";        
            }
        echo "</table>";
    }
    
    //////////////////
}
?>



        </div>
</body>
</html>