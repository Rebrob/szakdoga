<form action="" method="post" enctype="multipart/form-data">
    Select image to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload Image" name="submit"><br>
    Borító :<input type="radio" name="isCover" value="1"> <br>
    Sima :<input type="radio" name="isCover" value="0" checked>
</form>

<?php

if(isset($_POST["submit"])) 
{    
    if($_POST['isCover']==1)
    {    
    $target_dir = "images/names/cover/";
    }
    else
    {
    $target_dir = "images/names/base/";
    }
        
    $extension = pathinfo($_FILES['fileToUpload']['name'], PATHINFO_EXTENSION);
        echo $extension;
    $target_file = $target_dir . time() . ".$extension";
    $uploadOk = 1;

    if (file_exists($target_file))
    {       
        $uploadOk = 0;
    }

    if ($uploadOk == 0) 
    {

    }     
    else 
    {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) 
        {
            echo "The file ". $target_file. " has been uploaded.";
            
            $insert=1;        

            if($_POST['isCover']==1)
            {
                $stmt = $conn -> prepare
                ("
                SELECT imgID
                FROM name_images
                WHERE nameID=? AND isCover=1
                 ");
                $stmt -> bind_param( "i", $_GET['id']);
                $stmt -> execute();

                $result = $stmt -> get_result();

                if ( $result -> num_rows == 1 )  ///update
                {
                    $insert=0;

                    $stmt = $conn -> prepare
                    ("
                    UPDATE name_images
                    SET imgLocation=?
                    WHERE nameID=? AND isCover=1
                     ");
                    $stmt -> bind_param( "si", $target_file, $_GET['id']);
                    $stmt -> execute();    
                }

            }  

            if ($insert==1)
            {
                    $stmt = $conn -> prepare
                    ("
                    INSERT INTO name_images (nameID, isCover, imgLocation, imgAdded)
                    VALUES (?,?,?,NOW())
                     ");
                    $stmt -> bind_param( "iis", $_GET['id'], $_POST['isCover'], $target_file );
                    $stmt -> execute();
            }           
    } 

 }
    	header("Location: " . basename($_SERVER['PHP_SELF']) . "?id=" . $_GET['id']);
}
?>