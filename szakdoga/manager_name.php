<?php 
require_once("elements/dbConnect.php");
ob_start();
?>

<!DOCTYPE html>
<html>
    <head>     
        
       <?php require_once("elements/scriptLink.php"); ?>
        <script src="scripts/sliderVal.js"></script>
        <title>Megnézendők</title>
        
    </head>
    <body>   
        
       <?php require_once("elements/navbar.php"); ?>

 <!- ---------------------------------------------------------------------------------- ->
          
<div class="container">
 <div class="row justify-content-center">
                <div class="col-sm-6">   
                    <?php
///// chech admin
if(!isset($_SESSION['user']) or $_SESSION['user'] != 1  )
{    
    header("Location: login.php?error=1");
    die();
}
/////
if(isset($_GET['id']))  /// amennyiben id van megadva-> minden részlet
{   
    $id = $_GET['id'];
    
    require_once("nmParts/deleteImages.php"); ////////// genre image from movie    

    $stmt=$conn->prepare("
    SELECT nID, firstName, lastName, born, died, isFemale,dateAdded,bio from names where nID=?
    ;
    ");
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    if($result->num_rows == 1)
    {
    $row = $result->fetch_row();
    } 
    ?>
 <a class="text-center" href="manager_name.php">Új személy hozzáadása</a>
    <form action="<?php htmlentities($_SERVER['PHP_SELF'])?>" method="post">

    <input type="hidden" name="rejtett" value="<?php echo $id ;?>">    
        <b>Keresztnév:</b> <input class="form-control" type="text" name="editFirstName" value="<?php echo $row[1];?>" required> <br>
    <b>Vezetéknév:</b> <input class="form-control" type="text" name="editLastName" value="<?php echo $row[2];?>" required><br>
    <b>Született:</b> <input class="form-control" type="date" name="editBirth" value="<?php echo $row[3];?>" required><br>
    <b>Elhunyt:</b> <input class="form-control" type="date" name="editDeath" value="<?php echo $row[4];?>">    <br>
    <b>Nő:</b> <input type="checkbox" name="editGender" <?php if($row[5]==1) {echo "checked";}?>>    <br>    <br>   
    <b>Leírás:</b> <textarea class="form-control" name="editBio" ><?php echo nl2br($row[7]);?></textarea><br>
    <button class="btn btn-success" type="submit" name="editName">Szerkeszt</button>
    </form>

    <?php echo "<br><div class='bg-info'> Hozzáadva: ".$row[6]."</div><br>";
    echo "<hr>";

    
    	echo "<hr>";
     require_once("nmParts/loadImages.php"); 	/////////// load Képek
    	echo "<hr>";
     require_once("nmParts/insertImages.php"); 	/////////// Insert Képek

?>

<?php } else { //////Különben csak alap adatok ?>

    <form action="<?php htmlentities($_SERVER['PHP_SELF'])?>" method="post">
        
        <b> Keresztnév: </b><input class="form-control" type="text" name="newFirstName" required> <br>
        <b> Vezetéknév:</b> <input class="form-control"  type="text" name="newLastName" required><br>
        <b>Született:</b> <input class="form-control"  type="date" name="newBirth" required><br>

    <button class="form-control btn btn-success" type="submit" name="newName">Hozzáad</button>
    </form>

    <?php 
}

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    //////////////////////////////////////////////////////////// Szerkeszt név
    if (isset($_POST['editName']))
    {
    $nID=$_POST['rejtett']; //id 
        
    $efn= $_POST['editFirstName'];
    $eln= $_POST['editLastName'];
    $eb= $_POST['editBirth'];
    $ed=empty($_POST['editDeath']) ? NULL : $_POST['editDeath'];   
    $ebio=empty($_POST['editBio']) ? NULL : $_POST['editBio'];   
    $eg=(isset($_POST['editGender']) ? 1: 0);       
     
     echo $nID."<br>".$efn."<br>".$eln."<br>".$eb."<br>".$ed."<br>".$ebio."<br>".$eg."<br>";
     echo "----<br>";   
        ///// SQL update
        $newName=$conn->prepare
            ("
            UPDATE names SET firstName=?, lastName=?, born = ?, died = ?, bio = ?, isFemale=?, updatedOn=NOW() 
            WHERE nID=?;
            ");
        
        $newName->bind_param("sssssii",$efn,$eln,$eb,$ed,$ebio,$eg,$nID);
        $newName -> execute();
        ////////// update on
        header("Location: ".htmlentities($_SERVER['PHP_SELF'])."?id=".$nID);
    }
    //////////////////////////////////////////////////////////// 
    ///////////////////////////////////////////////////////////////// Új név
    
    else if (isset($_POST['newName']))
    {
        $nfn=trim($_POST['newFirstName']);
        $nln=trim($_POST['newLastName']);
        $nb=$_POST['newBirth'];
        
        
        $nexist=$conn->prepare
            ("
        SELECT nID
        FROM names
        WHERE firstName=? and lastName=? and born=?
            ");
        $nexist -> bind_param("sss", $nfn, $nln, $nb);
        $nexist -> execute();
        $nresult=$nexist -> get_result();
        
        if ($nresult->num_rows == 0)
        {
        //00//
       $addName = $conn->prepare
           ("
           INSERT INTO names (firstName, lastName, born, dateAdded) VALUES (?,?,?,NOW())
           ");        
        $addName->bind_param("sss", $nfn, $nln, $nb);        
        $addName->execute();
        
        echo $addName->error;
        $iid = $addName->insert_id;
        
        header("Location: ". htmlentities($_SERVER['PHP_SELF'])."?id=".$iid);
        //00//
        }
        
    }
    /////////////////
   
}
//////////////Képek hozzáadása
//////////////
?>
  </div>
    </div>
    </div>
             
        </body>    
</html>
