<?php
	if (isset($_POST['newMovie']))
	{
		$oTitle = trim($_POST['newTitleOriginal']);
		$nYear = trim($_POST['year']);

		$addMovie = $conn->prepare("
           INSERT INTO movies (titleOriginal, year, isSeries, movieDateAdded) VALUES (?,?,0,NOW())
           ");
		$addMovie->bind_param("si", $oTitle, $nYear);
		$addMovie->execute();

		echo $addMovie->error;
		$id = $addMovie->insert_id;

		header("Location: " . htmlentities($_SERVER['PHP_SELF']) . "?id=" . $id);
	}
?>