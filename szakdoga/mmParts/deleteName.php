<?php
if (isset($_GET['mndID']))
    {
        $delID = $_GET['mndID'];
         //////////////delete szerep
        $delQuery = $conn->prepare("
            DELETE FROM actor_role
            WHERE m_nID=?
            ");

        $delQuery->bind_param("i", $delID);
        $delQuery->execute();

       
        $delQuery = $conn->prepare("
        DELETE FROM movie_names WHERE mnID=?
        ");
        $delQuery->bind_param("i", $delID);
        $delQuery->execute();
       
        header("Location: " . basename($_SERVER['PHP_SELF']) . "?id=" . $_GET['id']);
    }
?>
