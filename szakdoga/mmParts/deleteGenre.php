<?php
	if (isset($_GET['mgdID']))
	{
		$delID = $_GET['mgdID'];

		$delQuery = $conn->prepare("
        DELETE FROM movie_genres WHERE mgID=?
        ");
		$delQuery->bind_param("i", $delID);
		$delQuery->execute();

		header("Location: " . basename($_SERVER['PHP_SELF']) . "?id=" . $_GET['id']);
	}
?>
