<?php
if (isset($_POST['editMovie']))
	{
		$mID = $_POST['rejtett']; //id
		$eht = empty($_POST['editHunTitle']) ? NULL : $_POST['editHunTitle'];
		$eot = $_POST['editOriginalTitle'];
		$ey = $_POST['editYear'];
		$eh = empty($_POST['runtime']) ? NULL : $_POST['runtime'];
		$etl = empty($_POST['trailerLink']) ? NULL : $_POST['trailerLink'];
		$ed = empty($_POST['description']) ? NULL : $_POST['description'];
		$es = (isset($_POST['isSeries']) ? 1 : 0);
		$eal = empty($_POST['editAge']) ? NULL : $_POST['editAge'];

		echo $mID . "<br>" . $eht . "<br>" . $eot . "<br>" . $ey . "<br>" . $eh . "<br>" . $etl . "<br>" . $ed . "<br>" . $es . "<br>";
		echo "----<br>";

		$stmt = $conn->prepare("
        UPDATE movies
        SET titleHun = ?, titleOriginal = ?, year = ?, runtime = ?, trailerLink = ?, isSeries = ?, description = ?, updatedOn = NOW(), ageLimit=?
        WHERE mID = ?;
         ");
		$stmt->bind_param("ssiisisii", $eht, $eot, $ey, $eh, $etl, $es, $ed, $eal, $mID);
		$stmt->execute();

		if ($es == 1)
		{
			$ens = empty($_POST['editNumOfSeasons']) ? NULL : $_POST['editNumOfSeasons'];
			$ene = empty($_POST['editNumOfEpisodes']) ? NULL : $_POST['editNumOfEpisodes'];

			$stmt = $conn->prepare("
             REPLACE INTO series  (series.movieID, series.sNumOfSeasons, series.sNumOfEpisodes)
             VALUES 
             (?,?,?)
             ");
			$stmt->bind_param("iii", $mID, $ens, $ene);
			$stmt->execute();
		}
		else
		{
			$stmt = $conn->prepare("
             DELETE FROM SERIES 
             WHERE movieID = ?
             ");
			$stmt->bind_param("i", $mID);
			$stmt->execute();
		}
		header("Location: " . htmlentities($_SERVER['PHP_SELF']) . "?id=" . $mID);
	}
?>