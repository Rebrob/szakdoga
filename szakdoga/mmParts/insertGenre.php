<?php
if (isset($_POST['addGenre']))
	{
		echo $_POST['mID'] . "<br>";
		echo $_POST['gID'] . "<br>";

		$genreAdd = $conn->prepare("
            INSERT INTO movie_genres (movieID, genreID, dateAdded)
            VALUES (?,?,NOW())
            ");

		$genreAdd->bind_param("ii", $_POST['mID'], $_POST['gID']);
		$genreAdd->execute();

		header("Location: " . htmlentities($_SERVER['PHP_SELF']) . "?id=" . $_POST['mID']);
	}
?>