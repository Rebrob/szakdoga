<form action="" method="post" enctype="multipart/form-data">
    Feltöltendő kép:
    <input class="btn btn-success" type="file" value="szia" name="fileToUpload" id="fileToUpload">
    <button class="btn btn-success" type="submit" value="Kép feltöltése" name="submit">Feltölt</button><br>
    Borító :<input type="radio" name="isCover" value="1"> <br>
    Sima :<input type="radio" name="isCover" value="0" checked>
</form>

<?php

if(isset($_POST["submit"])) 
{    
    if($_POST['isCover']==1)
    {    
    $target_dir = "images/movies/cover/";
    }
    else
    {
    $target_dir = "images/movies/base/";
    }
        
    $extension = pathinfo($_FILES['fileToUpload']['name'], PATHINFO_EXTENSION);
        echo $extension;
    $target_file = $target_dir . time() . ".$extension";
    $uploadOk = 1;

    if (file_exists($target_file))
    {       
        $uploadOk = 0;
    }

    if ($uploadOk == 0) 
    {

    }     
    else 
    {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) 
        {
            echo "A fájl ". $target_file. " feltöltve.";
            
            $insert=1;        

            if($_POST['isCover']==1)
            {
                $stmt = $conn -> prepare
                ("
                SELECT imgID
                FROM movie_images
                WHERE movieID=? AND isCover=1
                 ");
                $stmt -> bind_param( "i", $_GET['id']);
                $stmt -> execute();

                $result = $stmt -> get_result();

                if ( $result -> num_rows == 1 )  ///update
                {
                    $insert=0;

                    $stmt = $conn -> prepare
                    ("
                    UPDATE movie_images
                    SET imgLocation=?
                    WHERE movieID=? AND isCover=1
                     ");
                    $stmt -> bind_param( "si", $target_file, $_GET['id']);
                    $stmt -> execute();    
                }

            }  

            if ($insert==1)
            {
                    $stmt = $conn -> prepare
                    ("
                    INSERT INTO movie_images (movieID, isCover, imgLocation, imgAdded)
                    VALUES (?,?,?,NOW())
                     ");
                    $stmt -> bind_param( "iis", $_GET['id'], $_POST['isCover'], $target_file );
                    $stmt -> execute();
            }           
    } 

 }
    	header("Location: " . basename($_SERVER['PHP_SELF']) . "?id=" . $_GET['id']);
}
?>