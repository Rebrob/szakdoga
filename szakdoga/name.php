<!DOCTYPE html>
<html>
    <head>
        <?php require_once("elements/scriptLink.php"); ?>
    </head>    
    <body>
    <?php 
        require_once("elements/navbar.php"); 
        
        ?>
       <div class="container">
            <div class="row">
                <div class="col-sm-6">  
        
<?php
            if (isset($_SESSION['user']) and $_SESSION['user']==1)
            {
                echo "<a href='manager_name.php?id=".$_GET['id']."'>Szerkeszt</a><br><br>";
            }
                    
    require_once("elements/dbConnect.php");
    ob_start();

    if (isset($_GET['id']))
    {  $id=$_GET['id'];  

        $stmt =$conn->prepare( "SELECT * FROM names WHERE nID= ? ;");
        $stmt->bind_param("i",$id);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows == 1)
        {
           $row = $result->fetch_assoc(); 

                echo "<h1>".$row['firstName']." ".$row['lastName']."</h1><br>";
            echo "<table class='table'>";   
               echo "<tr><td><b>Keresztnév: </b></td><td>".$row['firstName']."</td></tr>";
               echo "<tr><td><b>Vezetéknév: </b></td><td>".$row['lastName']."</td></tr>";
               echo "<tr><td><b>Születési év: </b></td><td>".$row['born']."</td></tr>";

                if(!empty($row['died']))
                {
                   echo "<tr><td><b>Elhunyt: </b></td><td>".$row['died']."</td></tr>";
                }

                $nem="férfi";
                if($row['isFemale']==0)
                {
                      echo "<tr><td><b>Nem: </b></td><td>".$nem."</td></tr>";
                }
                else
                {
                      echo "<tr><td><b>Nem: </b></td><td> Nő</td></tr>";
                }        

               echo "<tr><td><b>Leírás: </b></td><td>".$row['bio']."</td></tr>";
            echo "</table>";

                $pageTitle=$row['firstName']." ".$row['lastName'];            
                  ?> <script> document.title = '<?php echo $pageTitle ?>'; </script>        <?php

            require_once("nParts/loadImages.php");  //képek betöltése
           ?>
                    </div>
                    <div class="col-sm-6 text-center">  
            <?php
            require_once("nParts/loadCover.php");  //Borító betöltése
            echo "<br><h2>Munkásságai:</h2>";
            require_once("nParts/loadCrew.php");   /////////////////STÁBLISTA LOAD
          ?>
                    </div>
            </div>
                    <div class="row justify-content-center">
                        <div class="col-sm-6">
        <?php
            if (isset($_SESSION['user']))
            {   ?>
                        <br><br>
                        <h3>Új hozzászólás:</h3>
                        <div class="form-group">
                            <form action="" method="post">
                                <textarea class="form-control" name="addComment"></textarea>
                                <br>
                                <button class="btn btn-success" type="submit" name="addSubmit">Hozzászól</button>
                            </form>
                        </div>
                <?php
            }

            echo "<br><b>Hozzászólások:</b> <br>";
           require_once("nParts/loadComments.php");   ///// comment load

        } 
        else 
        {
              header("Location:404.php?err=pnf");
        }

        require_once("nParts/insertComment.php");        //komment beszúrása

        $conn->close();
    }
    ?>         
                    </div>
                </div>
           </div>
        </body>
</html>