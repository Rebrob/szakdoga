<!DOCTYPE html>
<html>
    <head>

  <?php require_once("elements/scriptLink.php"); 
        require_once("elements/dbConnect.php");?>
    </head>
    
    <body>
 <?php 
        require_once("elements/navbar.php"); 
        ?>
          <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-6"> 
        <?php
if (isset($_SESSION['user']))
{
    ///////////Kilistázás + TÖRLÉS
    $uid = $_SESSION['user'];
    
    $stmt=$conn->prepare("
    SELECT movies.titleOriginal, movies.titleHun, ratings.rating, movies.mID, ratings.ratingDateAdded
    FROM ratings
    INNER JOIN movies ON movies.mID=ratings.movieID
    WHERE ratings.userID=?
    ORDER BY ratings.ratingDateAdded DESC
    ");
    $stmt->bind_param("i", $_SESSION['user']);
    $stmt->execute();
    $result=$stmt->get_result();  
    
    if ($result->num_rows > 0)
    {
        echo "<table class='table'>";   
        echo "<th></th><th>Film</th><th>Értéklelés</th><th>Értékelve</th>";
        while ($row = $result->fetch_row())
        {     
            
            
            $lang = empty($row[1]) ? $row[0] : $row[1];
         
            $cImg = $conn -> prepare
                    ("
                    SELECT imgID, imgLocation, imgAdded
                    FROM movie_images
                    WHERE movieID=? and isCover=1
                     ");
                    $cImg -> bind_param( "i", $row[3]);
                    $cImg -> execute();

                    $resultmi = $cImg -> get_result();

                    if ( $resultmi -> num_rows == 1 )
                    {
                         $crow = $resultmi -> fetch_row();	

                         echo "<tr><td><a href='movie.php?id=".$row[3]."'><img class='img-fluid' src='$crow[1]' width='80'></a></td>";
                    }
                    else
                    {
                        echo "<tr><td><a href='movie.php?id=".$row[3]."'><img class='img-fluid' src='images/movie-dummy.jpg' width='80'></a></td>";
                        
                    }
            
        echo "<td><a href='movie.php?id=".$row[3]."'>".$lang."</a></td><td>";
        echo $row[2]." %</td><td>".$row[4]."</td></tr>";
       
        }
        echo "</table>";
    }
    else
    {
        echo "<div class='bg-warning text-center'>Nincs még film érékelve!</div>";
    }
    //////////
    
}
else
{
    header("Location: login.php?error=1");
}
$conn->close();

?>
                   
           </div>
           </div>
           </div>
        </body>
</html>