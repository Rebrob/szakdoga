<?php
echo "<br><b>Kategóriák:</b><br> ";
        $stmt = $conn->prepare("
        SELECT genres.gName
        FROM 	((movie_genres
            INNER JOIN genres ON genres.gID = movie_genres.genreID)
            INNER JOIN movies ON movies.mID = movie_genres.movieID)
            WHERE movies.mID=?;
        ");

        $stmt->bind_param("i", $id);
        $stmt->execute();

        $result = $stmt->get_result();
        if ($result->num_rows>0)
        {
            while ($row=$result->fetch_row())
            {
                echo $row[0]."<br>";
            }
        }
        else
        {
            echo "<br>Nincs még kategória megadva.<br>";
        }    
        echo "<br>";
?>
