<?php
        echo "<br><h2>Hozzászólások:</h2> <br>";

        $stmt = $conn->prepare("
        SELECT accounts.uName, movie_comments.userID, movie_comments.commentText, movie_comments.commentDate
        FROM movie_comments
        INNER JOIN accounts ON accounts.uID=movie_comments.userID
        WHERE movie_comments.movieID=?
        ORDER by movie_comments.commentDate DESC, movie_comments.movieID 
        ");

        $stmt->bind_param("i", $id);
        $stmt->execute();

        $result = $stmt->get_result();
       
            
        if ($result->num_rows>0)
        {
        
            while ($row=$result->fetch_row())
            {
                echo "<table class='table table-bordered'><tr class='d-flex'><td class='col-sm-8'>";
               
                echo "<b>".$row[0]."</b></td><td class='col-sm-4'> ".$row[3]."</td></tr><tr><td colspan='2'>";
                echo nl2br($row[2]."<br>")."</td></tr>";
                echo "</table><br>";
                
            }
        }
        else
        {
            echo "<br>Nincs még hozzászólás a filmhez.<br>";
        }    
        
?>
