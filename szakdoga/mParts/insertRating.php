<?php
if(isset($_POST['sendRating']))
             {
                 $rating = $_POST['rating'];
                 
                 if ($rating!=0)
                 {                    
                     if ($rateResult->num_rows==0)
                     {
                         $addRating = $conn->prepare
                             ("
                             INSERT INTO ratings (userID, movieID, rating, ratingDateAdded)

                             VALUES (?,?,?,NOW())
                             ");
                         $addRating->bind_param("iii",$_SESSION['user'],$_GET['id'], $rating );
                         $addRating->execute();
                     }
                     else
                     {
                        $uRating = $conn->prepare
                             ("
                             UPDATE ratings 
                             SET rating=?, ratingDateAdded=NOW()
                             WHERE userID=? and movieID=?
                             ");
                         $uRating->bind_param("iii",$rating,$_SESSION['user'],$_GET['id']);
                         $uRating->execute();
                     }
                 }
                 else
                 {
                       $dRating = $conn->prepare
                             ("
                             DELETE FROM ratings 
                             WHERE userID=? and movieID=?
                             ");
                         $dRating->bind_param("ii",$_SESSION['user'], $_GET['id']);
                         $dRating->execute();
                 }
                 
                  header("Location: ". htmlentities($_SERVER['PHP_SELF'])."?id=".$_GET['id']);                 
             }   
?>
