<?php
		$stmt2 = $conn->prepare("
        SELECT AVG(rating), Count(rating) 
        FROM ratings
        WHERE movieID=?
        HAVING Count(rating) != 0
        ");
        $stmt2->bind_param("i", $id);
        $stmt2->execute();
        $result2 = $stmt2->get_result();
        
        echo "<b>Értékelés: </b>";
        
        if ($result2->num_rows > 0)            
        {       
        $row2 = $result2->fetch_row();	
            	
        echo floor($row2[0])."% (".$row2[1].")<br><br>";            
        }        
        else
        {
            echo "Nincs még értékelés.<br><br>";
        }
?>
