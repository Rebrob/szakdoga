<?php
 $stmt=$conn->prepare("
            SELECT wID
            FROM watchlist
            WHERE movieID = ?
            AND
            userID = ?;
            ");

            $stmt->bind_param("ii", $id,$_SESSION['user']);
            $stmt->execute();
            $result=$stmt->get_result();        

            // echo $result->num_rows;
            echo "<div id='resp'>";
            if(($result->num_rows) == 0)       // Ha nincs watchlisthez adva     
            {   
                echo "<br><br><button class='btn btn-success text-center' id='watch'>MEGNÉZENDŐHÖZ AD </button><br><br>";
            }
            else
            {
                echo "<br><br><div class='bg-success'>Megnézendőkhöz hozzáadva!</div><br><br>";
            }
             echo "</div>";      
?>
