<?php
    $ncImg = $conn -> prepare
    ("
    SELECT imgID, imgLocation, imgAdded
    FROM movie_images
    WHERE movieID=? and isCover=0
     ");
    $ncImg -> bind_param( "i", $_GET['id']);
    $ncImg -> execute();

    $result = $ncImg -> get_result();

    if ( $result -> num_rows != 0 )
    {
        while ( $ncrow = $result -> fetch_row() )
        {
            echo "<div class='text-center'>";
            echo "<img class='img-fluid' src='$ncrow[1]' width='90%'>  <br> <br>";
            echo "</div>";
        }
    }
?>
