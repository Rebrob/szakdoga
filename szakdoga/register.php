<!DOCTYPE html>
<html>
    <head>
        <title>Regisztáció</title>
<?php require_once("elements/scriptLink.php"); ?>
    </head>
    
    <body>
 <?php 
        require_once("elements/navbar.php"); 
         if (isset($_SESSION['user']))
            {
                die();
            }
        ?>
       <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-6">  
        
<?php
require_once("elements/dbConnect.php");

function input_check($input) 
{   
    if (strlen($input) >= 5 and strlen($input) <= 32)
    {
        $original = $input;

        if (trim($input) != $original) {return false;};
        if (stripslashes($input) != $original) {return false;};
        if (htmlspecialchars($input) != $original) {return false;};
        return true;    
    }
    else
    {
        return false;
    }
}


function checkPassword($p1, $p2)
{
    if ($p1 == $p2)
    {
     
        if (strlen($p1) >= 6 and strlen($p1) <= 32)    
        {            
            return true;    
        }
        else    
        {   
            return false;    
        }     
    }    
    else       
    {   
        return false;   
    }   
}

function checkMail ($mail)
{
    if (strlen($mail) >= 5 and strlen($mail) <= 50) 
    {     
        if (strpos($mail, '@') !== false and strpos($mail, '.') !== false )
        {
            return true;
        }
        else   
        {
            return false;
        }
    }
}
    
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
  $name = $_POST["username"];
  $email = $_POST["email"];    
  $password1 = $_POST["passw1"];
  $password2 = $_POST["passw2"];    
  $born = $_POST["bornDate"];    
  $genderValue = $_POST["gender"];
    
    
    $checkEx = $conn -> prepare("SELECT uName, uMail from accounts WHERE uName = ? OR uMail = ? ");
    $checkEx -> bind_param("ss", $name, $email);
    $checkEx -> execute();
    $resCheck = $checkEx -> get_result();
    
    if($resCheck->num_rows == 0 and checkPassword($password1, $password2))
    {   
           if(input_check($name) and checkMail($email))
           {
            $pw_hashed = password_hash($password1, PASSWORD_DEFAULT);

            $stmt = $conn -> prepare
            ("
            INSERT INTO accounts (uName, uPwd, regDate, uBorn, isFemale, uMail)
            VALUES (?,?,NOW(),?,?,?)
             ");
            $stmt -> bind_param( "sssis",$name, $pw_hashed, $born, $genderValue, $email );
            $stmt -> execute();    

               echo "<div class='bg-success text-center'>Sikeres regisztáció!</div>";
           }
    }
    else
    {
        echo "<div class='bg-warning text-center'>Sikertelen regisztáció!</div>";
    }
   }
    


?>


<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">

    <b>Felhasználónév:</b> <input class="form-control" type="text" name="username"  required> <br>
    <b>E-Mail: </b><input class="form-control" type="email" name="email"  required> <br>
   <b> Jelszó:</b> <input class="form-control" type="password" name="passw1"  required> <br>
   <b> Jelszó mégegyszer: </b><input class="form-control" type="password" name="passw2"  required> <br>
   <b> Születési dátum:</b> <input class="form-control" type="date" name="bornDate"  required> <br>
    
    
    <b>Nem: </b><br>
    <div class="form-check form-check-inline">
<input class="form-check-input" type="radio" name="gender" value="0" checked> <b>Férfi</b><br>
  
</div>
   
        <div class="form-check form-check-inline">
<input class="form-check-input" type="radio" name="gender" value="1"><b> Nő</b><br>

</div>
    
  <br>
    <br>
    <button class="btn btn-success form-control" type="submit" value="Submit">Regisztrál</button> <br>
    
</form>
                    
                               </div>
           </div>
           </div>
        </body>
</html>
