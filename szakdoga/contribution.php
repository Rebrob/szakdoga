<!DOCTYPE html>
<html>
    <head>
<?php require_once("elements/scriptLink.php"); ?>
<title>Film beküldése</title>
    </head>
    
    <body>
<?php 
require_once("elements/navbar.php");

if(!isset($_SESSION['user']))
{
	echo "a";
	header("Location: login.php?error=1");
	die();
}

 ?>

<div class="container">
     <div class="row justify-content-center">
         <div class="col-sm-6   ">

  <form action="<?php htmlentities($_SERVER['PHP_SELF']) ?>" method="post">   
    <b>Cím:</b> <input class="form-control" type="text" name="newTitle" required><br>
    <b>Év:</b> <input class="form-control" type="number" name="newYear" required><br>    
    <button class="btn btn-success form-control" type="submit" name="newContribution">Beküld</button>
    </form>
<?php
if(isset($_POST['newContribution']))
{
	require_once("elements/dbConnect.php");
	$title = htmlspecialchars(trim($_POST['newTitle']));
	$year = htmlspecialchars(trim($_POST['newYear']));
		
	if(empty($title) or $year > (date("Y")+5))
	{
	echo "<div class='bg-warning' >Hibás adatok!</div>";
	}
	else
	{
		$stmt = $conn -> prepare
		("
		INSERT INTO contributed (title, year, userID, added)
		VALUES (?,?,?,NOW());
		 ");
		$stmt -> bind_param( "sii",$title, $year, $_SESSION['user']);
		$stmt -> execute();
		

			echo $stmt->error;
		
	}
}
?>
        </div>
         </div>
    </div>
        </body>
</html>
