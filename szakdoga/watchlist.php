<?php 
require_once("elements/dbConnect.php");

?>

<!DOCTYPE html>
<html>
    <head>     
        
       <?php require_once("elements/scriptLink.php"); ?>
        <script src="scripts/sliderVal.js"></script>
        <title>Megnézendők</title>
        
    </head>
    <body>   
        
       <?php require_once("elements/navbar.php"); ?>

 <!- ---------------------------------------------------------------------------------- ->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-6">       

                    
<?php
if (isset($_SESSION['user']))
{
    ///////////Kilistázás + TÖRLÉS
    $uid = $_SESSION['user'];
    
    $stmt=$conn->prepare("
        SELECT watchlist.wID, movies.titleHun, movies.titleOriginal, watchlist.dateAdded, movies.mID
        FROM watchlist
        INNER JOIN movies on movies.mID=watchlist.movieID
        WHERE watchlist.userID=?;
        ");

            $stmt->bind_param("i", $uid);

            $stmt->execute();

            $result=$stmt->get_result();    
    
  
    
    if ($result->num_rows > 0)
    { 
        echo "<table class='table'>";   
        while ($row = $result->fetch_assoc())
        {     $wid=$row['wID'];
         $mID=$row['mID'];
         
         $lang;
         
         $cImg = $conn -> prepare
                    ("
                    SELECT imgID, imgLocation, imgAdded
                    FROM movie_images
                    WHERE movieID=? and isCover=1
                     ");
                    $cImg -> bind_param( "i", $row['mID']);
                    $cImg -> execute();

                    $resultmi = $cImg -> get_result();

                    if ( $resultmi -> num_rows == 1 )
                    {
                         $crow = $resultmi -> fetch_row();	

                         echo "<tr><td><a href='movie.php?id=".$mID."'><img src='$crow[1]' width='80'></a></td>";
                    }
                    else
                    {
                        echo "<tr><td><a href='movie.php?id=".$row['mID']."'><img src='images/movie-dummy.jpg' width='80'></a></td>";
                        
                    }
         
            echo "<div data-id='$wid'>";
            if (empty($row['titleHun'])) 
            {
                $lang = $row['titleOriginal'];
            }
            else
            {
                $lang = $row['titleHun'];
            }
            
         echo "<td><a href='movie.php?id=".$row['mID']."'>".$lang."</a></td>";
         
            echo "<td>".$row['dateAdded']."</td>";
         
         echo "<td><button class='deleteThis btn btn-success' data-id='$wid'>Törlés</button></td>";
         echo "</div></tr>";
        }
        echo "</table>";
    }
    else
    {
        echo "<div class='bg-warning text-center'>Nincs még film hozzáadva!</div>";
    }
    //////////
    
}
else
{
    header("Location: login.php?error=1");
}
$conn->close();

?>
<script>
    $(".deleteThis").click(
    function() {    
        var element=$(this);
    $.ajax
      ({
      type: "POST",
      url: 'watchlistAJAX.php',
      data: {wID: $(this).attr("data-id") }, 
      success: function( resp ) 
      {          
         
        location.reload();

      },
      error: function( req, status, err )           
      {          
        alert("Hiba!!");
        console.log( 'something went wrong', status, err );
      }
    });      
    });
</script>
                                        </div>
             </div>
            </div>
        </body>    
</html>
                    
 
         
   
