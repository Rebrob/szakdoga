<!DOCTYPE html>
<html>
    <head>
<title>Kezdőlap</title>
  <?php 
        require_once("elements/scriptLink.php"); 
        require_once("elements/dbConnect.php");
        ?>
    </head>
    
    <body>
 <?php 
        require_once("elements/navbar.php"); 
        ?>
          <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-6"> 
                    <?php
                    
                    //////Top 5 értékelt
                    
                    
        echo "<h2>Legjobbra értékelt filmek:</h2>";     
                    
        $stmt2 = $conn->prepare("
        SELECT AVG(rating) as rating, movies.titleHun, movies.titleOriginal, movies.mID, Count(rating)
        FROM ratings
        INNER JOIN movies on movies.mID=ratings.movieID
        group by movies.mID
        HAVING Count(rating) != 0
        ORDER BY AVG(rating) DESC
        LIMIT 5
        ");        
        $stmt2->execute();
        $result=$stmt2->get_result();
                    
    if ($result->num_rows > 0)
    {
         echo "<table class='table'>";   
        echo "<th></th><th>Film</th><th>Értéklelés</th>";
        while ($row = $result->fetch_row())
        {     
            
            
            $lang = empty($row[1]) ? $row[2] : $row[1];
         
            $cImg = $conn -> prepare
                    ("
                    SELECT imgID, imgLocation, imgAdded
                    FROM movie_images
                    WHERE movieID=? and isCover=1
                     ");
                    $cImg -> bind_param( "i", $row[3]);
                    $cImg -> execute();

                    $resultmi = $cImg -> get_result();

                    if ( $resultmi -> num_rows == 1 )
                    {
                         $crow = $resultmi -> fetch_row();	

                         echo "<tr><td><a href='movie.php?id=".$row[3]."'><img src='$crow[1]' width='80'></a></td>";
                    }
                    else
                    {
                        echo "<tr><td><a href='movie.php?id=".$row[3]."'><img src='images/movie-dummy.jpg' width='80'></a></td>";
                        
                    }
            
        echo "<td><a href='movie.php?id=".$row[3]."'>".$lang."</a></td><td>";
        echo floor($row[0])." % (".$row[4].")</td></tr>";
       
        }
        echo "</table>";
    }
                    //////
                    echo "<br><br><br>";
                    echo "<h2>Legújabb filmek:</h2>";
                      //////Top 5 legfrisebb
                    $stmt2 = $conn->prepare("
     SELECT movies.movieDateAdded, movies.titleHun, movies.titleOriginal, movies.mID
        FROM movies    
        ORDER BY movies.movieDateAdded DESC
        LIMIT 5
        ");
        
        $stmt2->execute();
                    $result=$stmt2->get_result();
    if ($result->num_rows > 0)
    {
         echo "<table class='table'>";   
        echo "<th></th><th>Film</th><th>Hozzáadva</th>";
        while ($row = $result->fetch_row())
        {     
            
            
            $lang = empty($row[1]) ? $row[2] : $row[1];
         
            $cImg = $conn -> prepare
                    ("
                    SELECT imgID, imgLocation, imgAdded
                    FROM movie_images
                    WHERE movieID=? and isCover=1
                     ");
                    $cImg -> bind_param( "i", $row[3]);
                    $cImg -> execute();

                    $resultmi = $cImg -> get_result();

                    if ( $resultmi -> num_rows == 1 )
                    {
                         $crow = $resultmi -> fetch_row();	

                         echo "<tr><td><a href='movie.php?id=".$row[3]."'><img src='$crow[1]' width='80'></a></td>";
                    }
                    else
                    {
                        echo "<tr><td><a href='movie.php?id=".$row[3]."'><img src='images/movie-dummy.jpg' width='80'></a></td>";
                        
                    }
            
        echo "<td><a href='movie.php?id=".$row[3]."'>".$lang."</a></td><td>";
        echo $row[0]."</td></tr>";
       
        }
        echo "</table>";
    }
                    //////
                    
                    $conn->close();

                    ?>
                   
           </div>
           </div>
           </div>
        </body>
</html>