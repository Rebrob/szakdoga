<?php
function generateRandomString( $length = 24, $set="lun" ) {   
    $characters='';    
    $set = strtolower($set);
    
    
    if( strpos( $set, 'hex' ) !== false ) {
        $characters.='0123456789abcdef';
    }
    else if( strpos( $set, 'bin' ) !== false ) {
        $characters.='01';
    } 
    else {
        if( strpos( $set, 'u' ) !== false ) {
            $characters.='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        if( strpos( $set, 'l' ) !== false ) {
            $characters.='abcdefghijklmnopqrstuvwxyz';
        }
        if( strpos( $set, 'n' ) !== false ) {
            $characters.='0123456789';
        }
    }
   
    $charactersLength = strlen( $characters );
    $randomString = '';
    
    for ( $i = 0; $i < $length; $i++ ) {
        $randomString .= $characters[rand( 0, $charactersLength - 1 ) ];
    }
    return $randomString;
}



require_once ( "elements/dbConnect.php" );

//$rand1, $rand2;
$start = microtime( true );

//mysqli_query( $conn, "START TRANSACTION" );
/*$stmt = $conn->prepare( "   
    INSERT INTO bigdata ( uuid )
    VALUES ( ? );     
    " );
*/
//$stmt->bind_param( "s", $rand2 );

$stmt = $conn->prepare("SELECT uuid FROM bigdata where uuid LIKE 'a%' LIMIT 1");

set_time_limit( 1800 );

$time_elapsed_secs = microtime( true ) - $start;

$inserted = 0;

while ( $inserted != ( 20 ) ) {
  
   // $rand2 = RandomString(  );
    $stmt->execute(  );
      $inserted++;
}

//mysqli_query( $conn, "COMMIT" );
echo "<br />" . $inserted . "<br />";
echo $stmt->error;
$time_elapsed_secs = microtime( true ) - $start;
echo "Ready: " . $time_elapsed_secs;


//echo generateRandomString(3,'lun');

?>