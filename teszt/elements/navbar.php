<nav class="navbar navbar-expand-lg navbar-dark bg-dark" >
  <a class="navbar-brand" href="index.php">Kezdőlap</a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navb">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="search.php">Kereső</a>
      </li>
        <?php if(isset($_SESSION['user']))
            { ?>
      <li class="nav-item">
        <a class="nav-link" href="watchlist.php">Megnézendők</a>
      </li>        
        <li class="nav-item">
        <a class="nav-link" href="ratings.php">Értékelések</a>
      </li>        
          <li class="nav-item">
        <a class="nav-link" href="contribution.php">Film beküldés</a>
      </li>      
               <?php if($_SESSION['user'] == 1){?>
        <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Admin
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="manager_movie.php">Új film</a>
        <a class="dropdown-item" href="manager_name.php">Új személy</a>
            </div>
    </li>
        <?php }} ?>
    </ul>
    <ul class="navbar-nav ml-auto">
        <?php if (!isset($_SESSION['user']))
              {?>
        <li class="nav-item">
        <a class="nav-link" href="login.php">Bejelentkezés</a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="register.php">Regisztráció</a>
      </li>
        <?php }  else  {?>              
        <li class="nav-item">
        <a class="nav-link" href="login.php?logout=1">Kijelentkezés</a>
      </li>
        <?php } ?>
      </ul>
   
    <form action="search.php" class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" name="search" placeholder="Film, személy">
      <button class="btn btn-success my-2 my-sm-0" type="submit">Keres</button>
    </form>
  </div>   
</nav>
<br>