-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2019 at 03:40 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `szakdolgozat`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `uID` int(11) NOT NULL,
  `uName` varchar(24) COLLATE utf8_hungarian_ci NOT NULL,
  `uPwd` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `regDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uBorn` date NOT NULL,
  `isFemale` int(11) NOT NULL,
  `uMail` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `isAdmin` int(11) NOT NULL,
  `uLastLogin` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`uID`, `uName`, `uPwd`, `regDate`, `uBorn`, `isFemale`, `uMail`, `isAdmin`, `uLastLogin`) VALUES
(1, 'admin', '$2y$10$GPKOnOHrY6TXmHyO6Kvi6uFYHMVKepIMKitAEXDM0dfJ21e1wTinW', '2019-01-24 15:24:06', '1998-12-26', 0, 'rebrob1998@gmail.com', 1, '2019-01-24 15:24:06'),
(2, 'user', '$2y$10$f0tjq6x1c7iL0qyFIlUGb.TwtN5x5ktvxAlYFwZ6438u0/VBHSL2K', '2019-01-24 15:26:57', '2000-01-01', 0, 'user@usermail.com', 0, NULL),
(3, 'child', '$2y$10$vLqBqr8wA/uS/So22TshiemebYqECJIRZAIumHDPiKxDrJu.jzbHa', '2019-01-24 15:29:31', '2010-01-01', 0, 'child@child.com', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `actors`
--

CREATE TABLE `actors` (
  `aID` int(11) NOT NULL,
  `aFirstName` varchar(60) COLLATE utf8_hungarian_ci NOT NULL,
  `aLastName` varchar(60) COLLATE utf8_hungarian_ci NOT NULL,
  `aBorn` date DEFAULT NULL,
  `aDied` date DEFAULT NULL,
  `isFemale` int(11) NOT NULL DEFAULT '0',
  `aBio` text COLLATE utf8_hungarian_ci,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `actors`
--

INSERT INTO `actors` (`aID`, `aFirstName`, `aLastName`, `aBorn`, `aDied`, `isFemale`, `aBio`, `dateAdded`) VALUES
(1, 'Orlando', 'Bloom', '1977-01-13', NULL, 0, NULL, '2019-01-24 17:53:32'),
(2, 'Christopher', 'Lee', '1922-05-27', '2015-01-07', 0, 'Sir Christopher Lee, teljes nevén: Christopher Frank Carandini Lee angol színész. Az 1940-es évek végétől filmezett. A Hammer Film Productions égisze alatt forgatott horrorfilmjeivel vált ismertté. Ő volt Drakula gróf egyik leghíresebb megformálója, de eljátszotta Frankenstein teremtményét is.', '2019-01-24 17:55:50'),
(3, 'Sean', 'Bean', NULL, NULL, 0, NULL, '2019-01-24 18:01:15'),
(4, 'Viggo', 'Mortensen', NULL, NULL, 0, NULL, '2019-01-24 18:01:15'),
(5, 'Liv', 'Tyler', '1977-07-01', NULL, 1, NULL, '2019-01-24 18:01:15'),
(6, 'Cate', 'Blanchett', NULL, NULL, 1, 'Cate Blanchett, teljes nevén Catherine Elise Blanchett kétszeres Oscar- és többszörös Golden Globe-díjas ausztrál színésznő és filmrendező.', '2019-01-24 18:01:15');

-- --------------------------------------------------------

--
-- Table structure for table `comments_actor`
--

CREATE TABLE `comments_actor` (
  `cID` int(11) NOT NULL,
  `actorID` int(11) NOT NULL,
  `commentText` text COLLATE utf8_hungarian_ci NOT NULL,
  `commentDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments_director`
--

CREATE TABLE `comments_director` (
  `cID` int(11) NOT NULL,
  `directorID` int(11) NOT NULL,
  `commentText` text COLLATE utf8_hungarian_ci NOT NULL,
  `commentDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments_movie`
--

CREATE TABLE `comments_movie` (
  `cID` int(11) NOT NULL,
  `movieID` int(11) NOT NULL,
  `commentText` text COLLATE utf8_hungarian_ci NOT NULL,
  `commentDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `directors`
--

CREATE TABLE `directors` (
  `dID` int(11) NOT NULL,
  `dFirstName` varchar(60) COLLATE utf8_hungarian_ci NOT NULL,
  `dLastName` varchar(60) COLLATE utf8_hungarian_ci NOT NULL,
  `dBorn` date DEFAULT NULL,
  `dDied` date DEFAULT NULL,
  `isFemale` int(11) NOT NULL DEFAULT '0',
  `dBio` text COLLATE utf8_hungarian_ci,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `directors`
--

INSERT INTO `directors` (`dID`, `dFirstName`, `dLastName`, `dBorn`, `dDied`, `isFemale`, `dBio`, `dateAdded`) VALUES
(1, 'Steven', 'Spielberg', '1946-12-18', NULL, 0, 'Steven Spielberg háromszoros Oscar-díjas amerikai filmrendező, producer, a Brit Birodalom lovagja, a legsikeresebb filmrendezők egyike.', '2019-01-24 18:10:48'),
(2, 'James', 'Cameron', '1954-08-16', NULL, 0, 'James Francis Cameron kanadai születésű Oscar-díjas amerikai rendező, forgatókönyvíró, vágó és producer. Az 1980-as évektől forgat egész estés játékfilmeket. Az Arnold Schwarzenegger főszereplésével készült Terminátor című futurisztikus akciófilmje tette világhírűvé. Alkotásai világszerte igen sikeresek voltak', '2019-01-24 18:10:48'),
(3, 'Peter', 'Jackson', '1961-10-31', NULL, 0, '', '2019-01-24 18:10:48');

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `gID` int(11) NOT NULL,
  `gName` varchar(30) COLLATE utf8_hungarian_ci NOT NULL,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`gID`, `gName`, `dateAdded`) VALUES
(1, 'Akció', '2019-01-24 17:11:54'),
(2, 'Kaland', '2019-01-24 17:11:54'),
(3, 'Sci-fi', '2019-01-24 17:11:54'),
(4, 'Fantasy', '2019-01-24 17:11:54'),
(5, 'Családi', '2019-01-24 17:11:54');

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `mID` int(11) NOT NULL,
  `mTitleHun` varchar(100) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `mTitleOriginal` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `mYear` int(11) NOT NULL,
  `mRuntime` time DEFAULT NULL,
  `isSeries` int(11) NOT NULL DEFAULT '0',
  `mTrailerLink` varchar(50) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mDesc` text COLLATE utf8_hungarian_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`mID`, `mTitleHun`, `mTitleOriginal`, `mYear`, `mRuntime`, `isSeries`, `mTrailerLink`, `dateAdded`, `mDesc`) VALUES
(1, 'A Gyűrűk Ura: A Gyűrű Szövetsége', 'The Lord of the Rings: The Fellowship of the Ring', 2001, '03:48:00', 0, 'watch?v=V75dMMIW2B4', '2019-01-24 18:15:07', NULL),
(2, 'Csillagok között', 'Interstellar', 2014, '02:49:00', 0, 'watch?v=zSWdZVtXT7E', '2019-01-24 18:17:41', 'A Csillagok között egy 2014-es tudományos-fantasztikus kalandfilm, amely felvonultatja a 21. századi elméleti fizika elképzeléseit a világűrről, a tér és idő viszonyáról, valamint felvázolja a zsákutcába jutott emberiség lehetséges fejlődési útját, jövőjét.'),
(3, 'Trónok harca', 'Game of Thrones', 2011, '00:57:00', 1, NULL, '2019-01-24 18:19:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `movie_actors`
--

CREATE TABLE `movie_actors` (
  `amID` int(11) NOT NULL,
  `movieID` int(11) NOT NULL,
  `actorID` int(11) NOT NULL,
  `role` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `dateAdded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `movie_actors`
--

INSERT INTO `movie_actors` (`amID`, `movieID`, `actorID`, `role`, `dateAdded`) VALUES
(1, 1, 1, 'Legolas', '2019-02-02 17:32:53'),
(2, 1, 2, 'Saruman', '2019-02-02 17:32:53'),
(3, 1, 3, 'Boromir', '2019-02-02 17:32:53'),
(4, 1, 6, 'Galadriel', '2019-02-02 17:32:53');

-- --------------------------------------------------------

--
-- Table structure for table `movie_directors`
--

CREATE TABLE `movie_directors` (
  `dmID` int(11) NOT NULL,
  `movieID` int(11) NOT NULL,
  `directorID` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `movie_directors`
--

INSERT INTO `movie_directors` (`dmID`, `movieID`, `directorID`, `dateAdded`) VALUES
(1, 1, 3, '2019-02-02 18:40:40');

-- --------------------------------------------------------

--
-- Table structure for table `movie_genres`
--

CREATE TABLE `movie_genres` (
  `gmID` int(11) NOT NULL,
  `movieID` int(11) NOT NULL,
  `genreID` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `movie_genres`
--

INSERT INTO `movie_genres` (`gmID`, `movieID`, `genreID`, `dateAdded`) VALUES
(1, 1, 4, '2019-02-02 19:01:00'),
(2, 1, 2, '2019-02-02 19:01:00');

-- --------------------------------------------------------

--
-- Table structure for table `pictures_actor`
--


-- --------------------------------------------------------

--
-- Table structure for table `series`
--

CREATE TABLE `series` (
  `seriesID` int(11) NOT NULL,
  `sNumOfEpisodes` int(11) DEFAULT NULL,
  `sNumOfSeasons` int(11) DEFAULT NULL,
  `movieID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE `votes` (
  `vID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `movieID` int(11) NOT NULL,
  `vRating` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `watchlist`
--

CREATE TABLE `watchlist` (
  `wID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `movieID` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`uID`);

--
-- Indexes for table `actors`
--
ALTER TABLE `actors`
  ADD PRIMARY KEY (`aID`);

--
-- Indexes for table `comments_actor`
--
ALTER TABLE `comments_actor`
  ADD PRIMARY KEY (`cID`),
  ADD KEY `actorID` (`actorID`);

--
-- Indexes for table `comments_director`
--
ALTER TABLE `comments_director`
  ADD PRIMARY KEY (`cID`),
  ADD KEY `directorID` (`directorID`);

--
-- Indexes for table `comments_movie`
--
ALTER TABLE `comments_movie`
  ADD PRIMARY KEY (`cID`),
  ADD KEY `movieID` (`movieID`);

--
-- Indexes for table `directors`
--
ALTER TABLE `directors`
  ADD PRIMARY KEY (`dID`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`gID`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`mID`);

--
-- Indexes for table `movie_actors`
--
ALTER TABLE `movie_actors`
  ADD PRIMARY KEY (`amID`),
  ADD KEY `movieID` (`movieID`),
  ADD KEY `actorID` (`actorID`);

--
-- Indexes for table `movie_directors`
--
ALTER TABLE `movie_directors`
  ADD PRIMARY KEY (`dmID`),
  ADD KEY `movieID` (`movieID`),
  ADD KEY `directorID` (`directorID`);

--
-- Indexes for table `movie_genres`
--
ALTER TABLE `movie_genres`
  ADD PRIMARY KEY (`gmID`),
  ADD KEY `movieID` (`movieID`),
  ADD KEY `genreID` (`genreID`);

--
-- Indexes for table `pictures_actor`
--


--
-- Indexes for table `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`seriesID`),
  ADD KEY `movieID` (`movieID`);

--
-- Indexes for table `votes`
--
ALTER TABLE `votes`
  ADD PRIMARY KEY (`vID`),
  ADD KEY `movieID` (`movieID`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `watchlist`
--
ALTER TABLE `watchlist`
  ADD PRIMARY KEY (`wID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `movieID` (`movieID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `uID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `actors`
--
ALTER TABLE `actors`
  MODIFY `aID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `comments_actor`
--
ALTER TABLE `comments_actor`
  MODIFY `cID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments_director`
--
ALTER TABLE `comments_director`
  MODIFY `cID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments_movie`
--
ALTER TABLE `comments_movie`
  MODIFY `cID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `directors`
--
ALTER TABLE `directors`
  MODIFY `dID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `gID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `mID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `movie_actors`
--
ALTER TABLE `movie_actors`
  MODIFY `amID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `movie_directors`
--
ALTER TABLE `movie_directors`
  MODIFY `dmID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `movie_genres`
--
ALTER TABLE `movie_genres`
  MODIFY `gmID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pictures_actor`
--

--
-- AUTO_INCREMENT for table `series`
--
ALTER TABLE `series`
  MODIFY `seriesID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `watchlist`
--
ALTER TABLE `watchlist`
  MODIFY `wID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments_actor`
--
ALTER TABLE `comments_actor`
  ADD CONSTRAINT `comments_actor_ibfk_1` FOREIGN KEY (`actorID`) REFERENCES `actors` (`aID`);

--
-- Constraints for table `comments_director`
--
ALTER TABLE `comments_director`
  ADD CONSTRAINT `comments_director_ibfk_1` FOREIGN KEY (`directorID`) REFERENCES `directors` (`dID`);

--
-- Constraints for table `comments_movie`
--
ALTER TABLE `comments_movie`
  ADD CONSTRAINT `comments_movie_ibfk_1` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`);

--
-- Constraints for table `movie_actors`
--
ALTER TABLE `movie_actors`
  ADD CONSTRAINT `movie_actors_ibfk_1` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`),
  ADD CONSTRAINT `movie_actors_ibfk_2` FOREIGN KEY (`actorID`) REFERENCES `actors` (`aID`);

--
-- Constraints for table `movie_directors`
--
ALTER TABLE `movie_directors`
  ADD CONSTRAINT `movie_directors_ibfk_1` FOREIGN KEY (`directorID`) REFERENCES `directors` (`dID`),
  ADD CONSTRAINT `movie_directors_ibfk_2` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`);

--
-- Constraints for table `movie_genres`
--
ALTER TABLE `movie_genres`
  ADD CONSTRAINT `movie_genres_ibfk_1` FOREIGN KEY (`genreID`) REFERENCES `genres` (`gID`),
  ADD CONSTRAINT `movie_genres_ibfk_2` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`);

--
-- Constraints for table `pictures_actor`
--

--
-- Constraints for table `series`
--
ALTER TABLE `series`
  ADD CONSTRAINT `series_ibfk_1` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`);

--
-- Constraints for table `votes`
--
ALTER TABLE `votes`
  ADD CONSTRAINT `votes_ibfk_1` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`),
  ADD CONSTRAINT `votes_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `accounts` (`uID`);

--
-- Constraints for table `watchlist`
--
ALTER TABLE `watchlist`
  ADD CONSTRAINT `watchlist_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `accounts` (`uID`),
  ADD CONSTRAINT `watchlist_ibfk_2` FOREIGN KEY (`movieID`) REFERENCES `movies` (`mID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
