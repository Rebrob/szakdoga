<!DOCTYPE html>
<html>
    <head>
        <title>Regisztáció</title>
<?php require_once("elements/scriptLink.php"); ?>
    </head>
    
    <body>
 <?php 
        require_once("elements/navbar.php"); 
         if (isset($_SESSION['user']))
            {
                die();
            }
        ?>
       <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-6">  
        
<?php
require_once("elements/dbConnect.php");

function input_check($input) 
{   
    if (strlen($input) >= 5 and strlen($input) <= 32)
    {
        $original = $input;

        if (trim($input) != $original) {return false;};
        if (stripslashes($input) != $original) {return false;};
        if (htmlspecialchars($input) != $original) {return false;};
        return true;    
    }
    else
    {
        return false;
    }
}


function checkPassword($p1, $p2)
{
    if ($p1 == $p2)
    {
     
        if (strlen($p1) >= 6 and strlen($p1) <= 32)    
        {            
            return true;    
        }
        else    
        {   
            return false;    
        }     
    }    
    else       
    {   
        return false;   
    }   
}

function checkMail ($mail)
{
    if (strlen($mail) >= 5 and strlen($mail) <= 50) 
    {     
        if (strpos($mail, '@') !== false and strpos($mail, '.') !== false )
        {
            return true;
        }
        else   
        {
            return false;
        }
    }
}
    
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    //$id = $_POST['id']; 
    $firstName = $_POST['firstName']; 
	$lastName = $_POST['lastName']; 
	$mail = $_POST['mail']; 
	$username = $_POST['username']; 
	$password1 = $_POST['password'];
    $pasword2 = $_POST['password2'];
	$bornDate = $_POST['bornDate']; 
	$regDate = 'CURRENT_TIMESTAMP'; 
	$lastLogin = 'CURRENT_TIMESTAMP';
    $zip = $_POST['zip'];
    $city = $_POST['city'];
    $street = $_POST['street'];
    $houseNumber = $_POST['houseNumber'];
    $floorDoor = $_POST['floorDoor'];
    $phone = $_POST['phone']; 
  $genderValue = $_POST["gender"];
    
    
    $checkEx = $conn -> prepare("SELECT username, mail from user WHERE username = ? OR mail = ? ");
    $checkEx -> bind_param("ss", $username, $mail);
    $checkEx -> execute();
    $resCheck = $checkEx -> get_result();
    
    if($resCheck->num_rows == 0 and checkPassword($password1, $password2))
    {   
           if(input_check($username) and checkMail($mail))
           {
            $pw_hashed = password_hash($password1, PASSWORD_DEFAULT);

            $stmt = $conn -> prepare
            ("
            INSERT INTO user (firstName, lastName, mail, username, password, bornDate, regDate, lastLogin, zip, city, street, houseNumber, floorDoor, phone, isFemale)
            VALUES (?,?,?,?,?,?,NOW(),?,?,?,?,?,?,?,?)
             ");
            $stmt -> bind_param( "ssssssssississi",$firstName,$lastName,$mail,$username,$pw_hashed, $bornDate, $regDate, $lastLogin, $zip, $city, $street, $houseNumber,$floorDoor,$phone, $genderValue  );
            $stmt -> execute();    

               echo "<div class='bg-success text-center'>Sikeres regisztáció!</div>";
           }
    }
    else
    {
        echo "<div class='bg-warning text-center'>Sikertelen regisztáció!</div>";
    }
   }
    


?>


<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">

    <!--<b>Felhasználónév:</b> <input class="form-control" type="text" name="username"  required> <br>
    <b>E-Mail: </b><input class="form-control" type="email" name="email"  required> <br>
   <b> Jelszó:</b> <input class="form-control" type="password" name="passw1"  required> <br>
   <b> Jelszó mégegyszer: </b><input class="form-control" type="password" name="passw2"  required> <br>
   <b> Születési dátum:</b> <input class="form-control" type="date" name="bornDate"  required> <br>-->
    <!--*************************************************************************************************************-->
    
    <div class="container"> 
	  <h2>Regisztráció</h2> 
	  <form name="felvitel" method="POST" action="beir.php" class="form-horizontal"> 
	  <div class="form-group"> 
	  <label class="control-label col-sm-2" for="firstName">Keresztnév:</label> 
	  <div class="col-sm-6"> 
	  <input type="text" class="form-control" id="firstName" name="firstName" 
	  placeholder="Adja meg a nevét!" required> 
	  </div> 
	  </div>
<div class="form-group"> 
	  <label class="control-label col-sm-2" for="lastName">Vezetéknév:</label> 
	  <div class="col-sm-6"> 
	  <input type="text" class="form-control" id="lastName" name="lastName" 
	  placeholder="Adja meg a nevét!" required> 
	  </div> 
	  </div> 
<div class="form-group"> 
	  <label class="control-label col-sm-2" for="mail">E-mail cím:</label> 
	  <div class="col-sm-6"> 
	  <input type="text" class="form-control" id="mail" name="mail" 
	  placeholder="Adja meg a címet!" required> 
	  </div> 
	  </div>
<div class="form-group"> 
	  <label class="control-label col-sm-2" for="username">Felhasználónév:</label> 
	  <div class="col-sm-6"> 
	  <input type="text" class="form-control" id="username" name="username" 
	  placeholder="Adja meg a felhasználónevet!" required> 
	  </div> 
	  </div>
<div class="form-group"> 
	  <label class="control-label col-sm-2" for="password">Jelszó:</label> 
	  <div class="col-sm-6"> 
	  <input type="password" class="form-control" id="password" name="password" 
	  placeholder="Adja meg a jelszavat!" required> 
	  </div> 
	  </div>   	  
	  
          <div class="form-group"> 
	  <label class="control-label col-sm-2" for="password2">Jelszó újra:</label> 
	  <div class="col-sm-6"> 
	  <input type="password" class="form-control" id="password2" name="password2" 
	  placeholder="Adja meg a jelszavat újra!" required> 
	  </div> 
	  </div>   	  
   
	<form name="felvitel" method="POST" action="modosit.php" class="form-horizontal"> 
   <div class="form-group"> 
   <label class="control-label col-sm-2" for="bornDate">Születési Dátum:</label> 
   <div class="col-sm-6"> 
   <input type="date" class="form-control" id="bornDate" name="bornDate" 
   placeholder="Adja meg a születésidátumot!" required>
   </div> 
   </div> 
        
        <div class="form-group"> 
	  <label class="control-label col-sm-2" for="phone">Telefonszám:</label> 
	  <div class="col-sm-6"> 
	  <input type="text" class="form-control" id="phone" name="phone" 
	  placeholder="Adja meg a telefonszámát!" required> 
	  </div> 
	  </div>
        
        <div class="form-group"> 
	  <label class="control-label col-sm-2" for="zip">Irsz:</label> 
	  <div class="col-sm-6"> 
	  <input type="number" class="form-control" id="zip" name="zip" 
	  placeholder="Adja meg az irányítószámot!" required> 
	  </div> 
	  </div>
        
        <div class="form-group"> 
	  <label class="control-label col-sm-2" for="city">Város:</label> 
	  <div class="col-sm-6"> 
	  <input type="text" class="form-control" id="city" name="city" 
	  placeholder="Adja meg a várost!" required> 
	  </div> 
	  </div>
        
        <div class="form-group"> 
	  <label class="control-label col-sm-2" for="street">Utca:</label> 
	  <div class="col-sm-6"> 
	  <input type="text" class="form-control" id="street" name="street" 
	  placeholder="Adja meg az utcát!" required> 
	  </div> 
	  </div>
        
        <div class="form-group"> 
	  <label class="control-label col-sm-2" for="houseNumber">Házszám:</label> 
	  <div class="col-sm-6"> 
	  <input type="number" class="form-control" id="houseNumber" name="houseNumber" 
	  placeholder="Adja meg a házszámot!" required> 
	  </div> 
	  </div>
        
        <div class="form-group"> 
	  <label class="control-label col-sm-2" for="floorDoor">Lépcsőház/Ajtó:</label> 
	  <div class="col-sm-6"> 
	  <input type="text" class="form-control" id="floorDoor" name="floorDoor" 
	  placeholder="Adja meg a lépcsőházat és az ajtót!" required> 
	  </div> 
	  </div>
    
    <!--*************************************************************************************************************-->
    
    <b>Nem: </b><br>
    <div class="form-check form-check-inline">
<input class="form-check-input" type="radio" name="gender" value="0" checked> <b>Férfi</b><br>
  
</div>
   
        <div class="form-check form-check-inline">
<input class="form-check-input" type="radio" name="gender" value="1"><b> Nő</b><br>

</div>
    
  <br>
    <br>
    <button class="btn btn-success form-control" type="submit" value="Submit">Regisztrál</button> <br>
    
</form>
                    
                               </div>
           </div>
           </div>
        </body>
</html>
