<link rel="stylesheet" type="text/css" href="../szakdoga/style/bootstrap.min.css">
<script src="../szakdoga/scripts/bootstrap.bundle.min.js"></script>
<script src="../szakdoga/scripts/jquery-3.3.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="margin:24px 0;">
  <a class="navbar-brand" href="javascript:void(0)">Kezdőlap</a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navb">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="javascript:void(0)">Kereső</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="javascript:void(0)">Megnézendők</a>
      </li>
        
        <li class="nav-item">
        <a class="nav-link" href="javascript:void(0)">Értékelések</a>
      </li>
        <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Admin
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="#">Film</a>
        <a class="dropdown-item" href="#">Személy</a>
              </div>
    </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
        <a class="nav-link" href="javascript:void(0)">Bejelentkezés</a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="javascript:void(0)">Regisztráció</a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="javascript:void(0)">Kijelentkezés</a>
      </li>
      </ul>
   
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Film, személy">
      <button class="btn btn-success my-2 my-sm-0" type="button">Keres</button>
    </form>
  </div>
</nav>
<?php

?>
